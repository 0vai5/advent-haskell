{-# LANGUAGE LambdaCase #-}

module Commons.Parsing (module Commons.Parsing, module Control.Applicative) where

-- Functional parsing library from chapter 13 of Programming in Haskell,
-- Graham Hutton, Cambridge University Press, 2016.

import Control.Applicative
import Data.Char
import qualified Data.Text as T

-- Basic definitions

newtype Parser a = P (T.Text -> [(a, T.Text)])

parse :: Parser a -> T.Text -> [(a, T.Text)]
parse (P p) = p

item :: Parser Char
item =
  P
    ( \inp ->
        [(T.head inp, T.tail inp) | not (T.null inp)]
    )

-- Sequencing parsers

instance Functor Parser where
  -- fmap :: (a -> b) -> Parser a -> Parser b
  fmap g p = P (fmap (\(v, out) -> (g v, out)) . parse p)

-- P
--   ( \inp -> case parse p inp of
--       [] -> []
--       [(v, out)] -> [(g v, out)]
--   )

instance Applicative Parser where
  -- pure :: a -> Parser a
  pure v = P (\inp -> [(v, inp)])

  -- <*> :: Parser (a -> b) -> Parser a -> Parser b
  pg <*> px =
    P
      ( \inp ->
          [(v, output) | (g, out) <- parse pg inp, (v, output) <- parse (fmap g px) out]
      )

-- P
--   ( \inp -> case parse pg inp of
--       [] -> []
--       [(g, out)] -> parse (fmap g px) out
--   )

instance Monad Parser where
  -- (>>=) :: Parser a -> (a -> Parser b) -> Parser b
  p >>= f =
    P
    (\inp ->
       [(b,output)| (a,out) <- parse p inp, (b,output) <- parse (f a) out]
    )

    -- P
    --   ( \inp -> case parse p inp of
    --       [] -> []
    --       [(v, out)] -> parse (f v) out
    --   )

-- Making choices

instance Alternative Parser where
  -- empty :: Parser a
  empty = P (const [])

  -- (<|>) :: Parser a -> Parser a -> Parser a
  p <|> q =
    P
      ( \inp -> case parse p inp of
          [] -> parse q inp
          xs -> xs
      )

-- Derived primitives

sat :: (Char -> Bool) -> Parser Char
sat p = do
  x <- item
  if p x then return x else empty

digit :: Parser Char
digit = sat isDigit

lower :: Parser Char
lower = sat isLower

upper :: Parser Char
upper = sat isUpper

letter :: Parser Char
letter = sat isAlpha

alphanum :: Parser Char
alphanum = sat isAlphaNum

char :: Char -> Parser Char
char x = sat (== x)

text :: T.Text -> Parser T.Text
text txt = fmap T.pack (string $ T.unpack txt) where
  string [] = return []
  string (x : xs) = do
    char x
    string xs
    return (x : xs)

ident :: Parser T.Text
ident = do
  x <- lower
  xs <- many alphanum
  return $ T.pack(x : xs)

nat :: (Num a, Read a) => Parser a
nat = do
  xs <- some digit
  return (read xs)

nats :: Parser [Int]
nats = do
  symbol "["
  n <- natural
  ns <- many (do symbol ","; natural)
  symbol "]"
  return (n:ns)

int :: Parser Int
int =
  do
    char '-'
    n <- nat
    return (- n)
    <|> nat

num :: (Num a, Read a) => Parser a
num =
  do
    char '-'
    n <- nat
    return (- n)
    <|> nat
-- Handling spacing

space :: Parser ()
space = do
  many (sat isSpace)
  return ()

token :: Parser a -> Parser a
token p = do
  space
  v <- p
  space
  return v

identifier :: Parser T.Text
identifier = token ident

natural :: Parser Int
natural = token nat

integer :: Parser Int
integer = token int

number :: (Num a, Read a) => Parser a
number = token num

symbol :: T.Text -> Parser T.Text
symbol xs = token (text xs)

parseWhile :: (a -> Bool) -> Parser a -> Parser [a]
parseWhile f parser = many aparser where
  aparser = do
    a <- parser
    if f a then pure a else empty

wordParser :: Parser String
wordParser = do
  space
  word <- some $ sat (not . isSpace)
  space
  return word

separatedBy :: Parser a -> Parser b -> Parser [a]
separatedBy pa sepBy = (:) <$> pa <*> many (sepBy *> pa)

ifThenElse :: Parser a -> (a -> Parser b) -> Parser b -> Parser b
ifThenElse pif pthen pelse = (pif >>= pthen) <|> pelse

newLine :: Parser ()
newLine = char '\n' >> return ()

nonSpace :: Parser String
nonSpace = some $ sat (not . isSpace)

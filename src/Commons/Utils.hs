{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE LambdaCase #-}
module Commons.Utils (readEntire,
                      parseIntegral,
                      startsWith,
                      arrLength,
                      listToArray,
                      powerSet,
                      permutations,
                      foldTerminate,
                      intReader,
                      alphabetsReader,
                      newLineReader
                     ) where

import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Text.Read as T
import qualified Data.Array as A
import qualified Data.Vector.Generic as VG
import qualified Data.Vector as VB
import Data.Foldable (foldl')
import Control.Monad (replicateM)
import Debug.Trace (trace)
import Data.List (scanl')
import qualified Text.ParserCombinators.ReadP as RP
import Data.Char (isDigit, isAlpha)

readEntire :: T.Reader a -> T.Text -> Either String a
readEntire reader txt =
  reader txt >>= \(a, t) ->
    if T.null t
      then Right a
      else Left $ "Error: Incomplete read - remaining " ++ T.unpack t


parseIntegral :: Integral a => T.Text -> Either String a
parseIntegral = readEntire $ T.signed T.decimal

startsWith :: (Eq a) => [a] -> [a] -> Bool
startsWith [] l = True
startsWith (p:prfxs) (x:xs) | p == x = startsWith prfxs xs
startsWith _ _ = False

arrLength :: A.Ix a => A.Array a b -> Int
arrLength = A.rangeSize . A.bounds

listToArray :: [a] -> A.Array Int a
listToArray xs = A.listArray (0,length xs) xs

permutations :: [a] -> [[a]]
permutations [] = []
permutations as = VG.toList <$> perms as l where
  l = length as
  perms _ 0 = [VB.empty]
  perms [] _ = [VB.empty]
  perms (x:xs) sz = [perm | sub <- perms xs (sz-1), let p0 = VG.singleton x VG.++ sub, perm <- foldl' (f p0) [] [0..sz-1]] where
        f p0 r i = swap p0 i 0 : r
        swap v i j = if i == j then v else v VG.// [(i, v VG.! j),(j, v VG.! i)]

powerSet [] = [[[]]]
powerSet (x:xs) = let next = powerSet xs
                            in zipWith (++)
                                ([]:next)
                                    ( map (map (x:)) next ++ [[]] )

foldTerminate :: (b -> a -> Either b b) -> b -> [a] -> b
foldTerminate f accum0 list0 =
    either id id (go accum0 list0)
  where
    go !accum = \case
          [] -> Left accum
          x:xs -> f accum x >>= flip go xs

intReader :: RP.ReadP Int
intReader = do
    digits <- RP.many1 $ RP.satisfy isDigit
    return $ read digits

alphabetsReader :: RP.ReadP String
alphabetsReader = RP.many1 $ RP.satisfy isAlpha

newLineReader :: RP.ReadP Char
newLineReader = RP.char '\n'

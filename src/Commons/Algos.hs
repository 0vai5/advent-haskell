{-# LANGUAGE BangPatterns #-}
module Commons.Algos where

import Data.List (foldl')
import qualified Data.Vector as VB
import qualified Data.Vector.Generic as VG
import Debug.Trace (trace)

-- Algo 2
-- this is my fav. easy to understand and faster and low memory!
-- in ghci it might appear slower but compiled version runs faster than other algos. i guess bec of fusion!
-- 1. generate in the order of input elements.
-- 2. instead of generating combination we generate indexes!
-- 3. since we generate in order, the previous generated indexes can be used to generate the next indexes.
-- 4. say we have k = 3 and input length = 5. Now say if we have indexes: 0,2,3, then next index would be 0,2,4
--    then 0,3,4 and then 1,2,3
-- 5. How to generate next index?
-- 6. Main idea is: next index will always be larger than the previous index. And maximum value of index can be l-1
--    where l is length of input. Notice that these two conditions combined means that first element max value can
--    be (l-1)-k. Bec only then the last element max index would be l-1.
-- 7. Thus we have maximum value possible for each position.
-- 7. So, here mkNext, does the job. but it generates indexes in the opposite order bec linked list are fast to read
--    from top. Thus in our eg above we have 3,2,0 then we generate 4,2,0 then 3,2,0
-- 8. To generate next, we keep going from left to right till we reach the position which is not the maximum value of that position.
-- 9. now, once we reach this position, all the remanining elements would remain same but the earlier elements will be changed. say
--    we changed the value m to m+1. now, the previous element must be m+1+1 and so on.

-- instead of take c, takeWhile can be used which is slightly slower but perhaps more simple.
combinations :: Int -> [a] -> [[a]]
combinations _ [] = []
combinations 0 xs = [[]]
combinations 1 xs = map pure xs
combinations k xs =
  let ps = [k -1, k -2 .. 0]
      l = length xs
      v = VB.fromList xs
      func = foldl' (\r i -> v VG.! i : r) []
      c = fromIntegral $ ncombs (fromIntegral l) (fromIntegral k)
   in if k > l then [] else if k == l then pure xs else map func . take c . iterate (mkNext l) $ ps

mkNext l xs = loop l xs
  where
    loop l' [] = []
    loop l' (a : as)
      | a' < l' = [b, b -1 .. a'] ++ as
      | otherwise = loop (l' -1) as
      where
        a' = a + 1
        b = a' + (l - l')
-- there are 3 algos for combinations. none of them are mine all from stack overflow.

-- algo1:
-- This took me good time to understand. i tried optimizing it further but results were more or less same.
-- this is complicated but provides not much advantage from the previous algo atleast while using lists.
-- possibly if using Seq it may run faster.

--1. generate combinations in the order of the given input. say "bdef" is input then one eg
--   combination of size 3 will be "bef" and not "fbe" or any other order.
--2. break the problem and find k-1 length combinations from n-1 length list.
--3. when we have this k-1 length combinations. we use it to create combs of k length. how to do it?
--4. just take each element of the input list and put it in all the combinations of k-1
--   length which /do not/ contain this input element.
--5. how to do this efficiently?
--6. since we are generating in the order of input elements. the k-1 combinations we have must be ordered.
--   thus if we have say 'b' first in our input then all the combinations of k-1 length that contains `b`
--   must be earlier and must be contigous.
--7. can we count them?
--8. yes. since we have k-1 length combinations from list on n-1 length, if we fix `b`, then all the
--   combinations containing b will be of length k-2 from the list of length n-2. Or n-2 `C` k-2
--9. that's the idea. now we do this for each element in input till?
--10. suppose we are generating 3 length combinations of input "abcde" there won't be any combination
--    starting with d or e. bec "deb" contains b but b must be in front
--11. so go only  until c or the kth element from the last.
--12. notice the use of a small optimization. we first need to drop n-2 C k-2 elements then n-3 C k-2...
--13. so we use n-1 C k = ((n-k) / n) * n C k
combs :: Int -> [a] -> [[a]]
combs k as@(x : xs)
  | k == 0 = [[]]
  | k == 1 = map pure as
  | k == l = pure as
  | k > l = []
  | otherwise = loop (l -2) (k -2) (fromIntegral $ ncombs (fromIntegral $ l-2) (fromIntegral $ k-2)) as $ combs (k -1) xs
  where
    l = length as
    loop n t nc ys cs
      | n < t = []
      | otherwise =
        let nc' = ((n-t)*nc) `div` n
            z:zs = ys
         in map (z :) cs ++ loop (n-1) t nc' zs (drop (fromIntegral nc) cs)


ncombs :: Integer -> Integer -> Integer
ncombs n k = product [(n - k + 1) .. n] `div` product [1 .. k]

-- this again is quite an elegant algo. and took me some time to understand.
-- this is ofcourse slower than previous ones.
-- also it does not generate in elements in the input order.
-- the idea is we generate powerSet of the input elements in the order of size.
-- i.e. first we get all sets of length l then l-1 ... till of length 0.
-- thus combinations of size k would be kth element from the end, or n-k from the beg.
-- how to compute powerset?
-- we can generate powerset of n+1 elements using powerset of n elements.
-- the first powerSet of n elements would be or length n then n-1 ..till 0.
-- if we put one element (n+1) element, in each of them then this power set would be of length n+1,n,n-1..1
-- it won't contain elements which do not contain (n+1) element. so, we combine powerSet of n elements with this new result in the order of size.
-- the power set of n elements do not have any set of length n+1 thus we put [] for it in the beg. Similarly, our result of n+1,n,n-1..1 do not contian
-- 0 length powerSet so we put empty for it too. That's it!

subsequencesOfSize :: Int -> [a] -> [[a]]
subsequencesOfSize k xs =
  let l = length xs
   in if k > l
        then []
        else powerSet xs !! (l - k)

powerSet [] = [[[]]]
powerSet (x:xs) = let next = powerSet xs
                            in zipWith (++)
                                ([]:next)
                                ( map (map (x:)) next ++ [[]] )
perms :: [a] -> [[a]]
perms xs = run len xs
  where
    len = length xs

    rotate :: [a] -> [a]
    rotate (x : xs) = xs ++ [x]

    rotations :: Int -> [a] -> [[a]]
    rotations l xs = take l (iterate rotate xs)

    run :: Int -> [a] -> [[a]]
    run _ [] = [[]]
    run _ [x] = [[x]]
    run n (x : xs) = run (n -1) xs >>= rotations n . (x :)

ls =
  [ (5, [1 .. 100]),
    (15, [1 .. 31]),
    (22, [1 .. 30])
  ]

test :: (Eq a, Num a) => (Int -> [a] -> [[a]]) -> [(Int, [a])] -> Bool
test f ls = all (\arg -> all (all (/= 9999)) (uncurry f arg)) ls

test1 = test subsequencesOfSize ls

test2 = test combs ls

test3 = test combinations ls

{-# LANGUAGE RecordWildCards #-}
module Advent.Y20.Day21 where

import qualified Commons.Parsing as P
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Data.Map(Map)
import qualified Data.Map as M
import Data.Set(Set)
import qualified Data.Set as S
import Commons.Algos (combinations)
import Commons.Utils (permutations)
import Data.Functor ((<&>))
import Data.List ((\\), foldl', sort, intercalate)

type Allergens = Set String
type Ingredients = Set String
data Food = Food {
  ingredients :: Ingredients,
  allergens :: Allergens
} deriving (Eq,Show)

parser :: P.Parser [Food]
parser = P.separatedBy foodp P.newLine where
  foodp = Food <$> ingredientsp <*> allergensp
  ingredientsp = S.fromList <$> P.separatedBy wordp (P.char ' ')
  csv = P.separatedBy wordp (P.text ", ")
  wordp = P.some P.letter
  allergensp = fmap S.fromList $ P.char ' ' *> P.text "(contains " *> csv <* P.char ')'

type AITable = Map String Ingredients

mkAllergensTable :: [Food] -> AITable
mkAllergensTable = foldl' f M.empty where
  f aiTbl Food{..} = S.foldl' (g ingredients) aiTbl allergens
  g ingredients aiTbl allgn = M.insertWith S.intersection allgn ingredients aiTbl

type AIM = Map String String

mkAllergensMap :: AITable -> [AIM]
mkAllergensMap aiTbl = go (M.keys aiTbl) S.empty M.empty where
  go :: [String] -> Ingredients -> AIM -> [AIM]
  go [] usedIngdts aim = [aim]
  go (allgn:remnAllgns) usedIngdts aim = [newAim |
                                          ingdt <- S.toList . S.difference (aiTbl M.! allgn) $ usedIngdts,
                                          newAim <- go remnAllgns (S.insert ingdt usedIngdts) (M.insert allgn ingdt aim)]

solve1 foods = rs where
  aim = head . mkAllergensMap . mkAllergensTable $ foods
  foodWithAllergens = S.fromList $ M.elems aim
  rs = sum $ fmap (S.size . flip S.difference foodWithAllergens . ingredients) foods

part1 = T.interact $ T.pack . show . solve1 . fst . head . P.parse parser

solve2 foods =  map (allgnsMap M.!) . sort . M.keys $ allgnsMap where
  allgnsMap = head . mkAllergensMap . mkAllergensTable $ foods

part2 = T.interact $ T.pack . show . intercalate ",". solve2 . fst . head . P.parse parser

{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

module Advent.Y20.Day08 where

import qualified Commons.Parsing as P
import Control.Monad (guard)
import Commons.Utils
import qualified Data.Array.Unboxed as A
import qualified Data.IntSet as S
import qualified Data.Text as T

data Ins = ACC Int | JMP Int | NOP Int deriving (Show)

type Code = A.Array Int Ins

data Status = LOOP | NOLOOP deriving (Eq, Show)

solve1 :: Code -> (Status, Int)
solve1 code = go S.empty 0 0
  where
    go vistd accum pc
      | pc >= arrLength code = (NOLOOP, accum)
      | S.member pc vistd = (LOOP, accum)
      | otherwise =
        let vistd' = S.insert pc vistd
         in case code A.! pc of
              NOP _ -> go vistd' accum (pc + 1)
              ACC v -> go vistd' (accum + v) (pc + 1)
              JMP ofst -> go vistd' accum (pc + ofst)

parser :: P.Parser Code
parser = fmap (\lst -> A.listArray (0, length lst - 1) lst) (P.some ins)
  where
    ins = fmap f P.wordParser <* P.space <* P.optional (P.char '+') <*> P.int <* P.space
    f "acc" = ACC
    f "jmp" = JMP
    f "nop" = NOP

part1 :: IO ()
part1 =
  interact $
    show
      . solve1
      . parseIt
  where
    parseIt str = fst $ head $ P.parse parser (T.pack str)

solve2 :: Code -> Int
solve2 code =
  head
    [ c | i <- [0 .. (arrLength code - 1)], Just (NOLOOP, c) <- [fmap solve1 (moded i code)]
    ]

moded :: Int -> Code -> Maybe Code
moded ix code = case code A.! ix of
  ACC _ -> Nothing
  JMP v -> Just $ code A.// [(ix, NOP v)]
  NOP v -> Just $ code A.// [(ix, JMP v)]

part2 :: IO ()
part2 =
  interact $
    show
      . solve2
      . parseIt
  where
    parseIt str = fst $ head $ P.parse parser (T.pack str)

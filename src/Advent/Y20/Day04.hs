{-# LANGUAGE LambdaCase #-}
module Advent.Y20.Day04 where

import qualified Commons.Parsing as P
import qualified Data.Text as T
import Data.Array.Unboxed
import GHC.Exts (IsString)
import GHC.Unicode (isSpace)
import qualified Data.Bifunctor
import qualified Data.Text.IO as T
import Data.List (foldl')

data Key = BY | IY | EY | HG | HC | EC | PI | CI deriving (Eq,Ord,Enum,Ix,Bounded,Show)

toKey :: (IsString a, Eq a) => a -> Key
toKey s  = case s of
  "byr" -> BY
  "iyr" -> IY
  "eyr" -> EY
  "hgt" -> HG
  "hcl" -> HC
  "ecl" -> EC
  "pid" -> PI
  "cid" -> CI
  _ -> error "Invalid Key"

testYear n lo hi val = length val == n && lo <= val && val <= hi

testInt str lo hi = (not.null) rs
                    && inRange (lo,hi) (fst $ head rs)
  where rs = P.parse P.nat str

testHgt ht = cm || inch where
  hgt = T.pack ht
  cm = T.isSuffixOf "cm" hgt && testInt hgt 150 193
  inch = T.isSuffixOf "in" hgt && testInt hgt 59 76

testHC str = (not.null) rs where
  txt = T.pack str
  rs = P.parse parser txt
  isHex d = inRange ('0','9') d || inRange ('a','f') d
  parser = do
    P.char '#'
    clr <- P.some $ P.sat isHex
    return (length clr == 6)


keyValidator :: Key -> String -> Bool
keyValidator = \case
               BY -> testYear 4 "1920" "2002"
               IY -> testYear 4 "2010" "2020"
               EY -> testYear 4 "2020" "2030"
               HG -> testHgt
               HC -> testHC
               EC -> flip elem ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
               PI -> \pi -> length pi == 9 && testInt (T.pack pi) 0 999999999
               CI -> const True

type PP = Array Key String

ppparser :: T.Text -> Either String PP
ppparser txt = if null rs
  then Left $ "Error in parsing: " ++ T.unpack txt
  else Right $ mkPP $ fst $ head rs where
  mkPP :: [(String,String)] -> PP
  mkPP kvs = accumArray (++) "" (BY,CI) $ map (Data.Bifunctor.first toKey) kvs
  rs = P.parse (P.many kvparser) txt
  kvparser = do
    P.space
    k <- P.some (P.sat (/= ':'))
    P.char ':'
    v <- P.some (P.sat (not.isSpace))
    return (k,v)

valid1 :: PP -> Bool
valid1 pp = all (\ix -> ix == CI || not (null (pp ! ix))) [minBound..maxBound]

part1 :: IO ()
part1 = T.interact $
  T.pack . show
  . fmap vc. traverse ppparser . T.splitOn "\n\n" where
  vc = foldl' (\tot pp -> tot + fromEnum (valid1 pp)) 0

valid2 :: PP -> Bool
valid2 pp = all (\ix -> keyValidator ix (pp ! ix)) [minBound..maxBound]

part2 :: IO ()
part2 = T.interact $
  T.pack . show
  . fmap vc. traverse ppparser . T.splitOn "\n\n" where
  vc = foldl' (\tot pp -> tot + fromEnum (valid2 pp)) 0

module Advent.Y20.Day03 where

import Data.Semigroup (Product(Product), getProduct)
import Data.Foldable (Foldable(foldl'))
import qualified Data.Text as T
import Data.Monoid (Sum(Sum))

solve1 :: Int -> Int -> [String] -> Int
solve1 r c input = length $ [True| (i,row) <- zinp, test i row] where
  zinp = zip [0..] input
  clength = length $ head input
  test i row =
    (i `mod` r == 0) && (row !! j == '#') where j = (i `div` r) * c `mod` clength

solve2 :: [(Int,Int)] -> [String] -> Int
solve2 slopes input = getProduct $ foldMap (\(r,c) -> Product $ solve1 r c input) slopes

part1 :: IO()
part1 = interact $ show . solve1 1 3 . lines

part2 :: IO()
part2 = interact $ show . solve2 [(1,1),(1,3),(1,5),(1,7),(2,1)] . lines

day3b :: T.Text -> Int
day3b input = a * b * c * d * e where
    (Sum a, Sum b, Sum c, Sum d, Sum e) = foldl' (<>) mempty
      [ ( Sum . count $ i
        , Sum . count $ 3 * i
        , Sum . count $ 5 * i
        , Sum . count $ 7 * i
        , case i `quotRem` 2 of
              (j, 0) -> Sum $ count j
              _ -> mempty
        )
      | (i, line) <- zip [0..] $ T.lines input
      , let count j = fromEnum $ T.index line (j `mod` T.length line) == '#'
      ]

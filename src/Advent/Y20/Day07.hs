{-# LANGUAGE InstanceSigs #-}
module Advent.Y20.Day07 where

import qualified Commons.Parsing as P
import Commons.Utils (startsWith)
import Data.Char
import Data.Foldable (foldl')
import Data.Functor (($>))
import Data.List.Split hiding (startsWith)
import qualified Data.Map as M
import Data.Maybe
import qualified Data.Set as S
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Text.Show as T

-- fst is outer bag and snd is inner bags
type Record = (String, [(Int, String)])

color :: P.Parser String
color =
  unwords
    <$> P.parseWhile (not . startsWith "bag") P.wordParser
    <* P.text "bag"
    <* P.optional (P.char 's')

bag :: P.Parser (Int, String)
bag = (,) <$> P.int <*> color

bags :: P.Parser [(Int, String)]
bags =
  bag `P.separatedBy` (P.char ',' *> P.space)
    P.<|> P.space *> P.text "no other bags" *> P.space $> [(0, "no other")]

recParser :: P.Parser Record
recParser = (,) <$> (color <* P.wordParser) <*> bags

recsParser :: P.Parser [Record]
recsParser = recParser `P.separatedBy` (P.char '.' <* P.space)

-- key are inner bag and values outer bag
type ContainedByMap = M.Map String [String]

addRec :: ContainedByMap -> Record -> ContainedByMap
addRec outers (val, keys) =
  foldl'
    (\map (k, v) -> M.insertWith (++) k v map)
    outers
    [(k, [val]) | (_, k) <- keys]

parser1 :: P.Parser ContainedByMap
parser1 = fmap f recsParser
  where
    f = foldl' addRec M.empty

solve1 :: ContainedByMap -> String -> S.Set String
solve1 cbm clr = S.delete clr $ go [clr] S.empty
  where
    go :: [String] -> S.Set String -> S.Set String
    go [] set = set
    go (x : xs) set =
      if x `S.member` set
        then go xs set
        else let outerBags = M.findWithDefault [] x cbm in go (xs ++ outerBags) (S.insert x set)

part1 :: IO ()
part1 =
  interact $
    show
      . S.size
      . flip solve1 "shiny gold"
      . parseIt
  where
    parseIt str = fst $ head $ P.parse parser1 (T.pack str)

type ContainsMap = M.Map String [(Int, String)]

parser2 :: P.Parser ContainsMap
parser2 = fmap M.fromList recsParser

solve2 :: ContainsMap -> String -> Int
solve2 cntnr clr = go 1 clr - 1
  where
    go 0 clr = 0
    go n clr = n + n * sum [go n' clr' | (n', clr') <- fromMaybe [] $ cntnr M.!? clr]

part2 :: IO ()
part2 =
  interact $
    show . flip solve2 "shiny gold" . parseIt
  where
    parseIt str = fst $ head $ P.parse parser2 (T.pack str)

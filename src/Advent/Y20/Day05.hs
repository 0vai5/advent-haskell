module Advent.Y20.Day05 where

import Data.Foldable (foldl')

type Range = (Int, Int)

data Dir = H | L deriving (Eq)

type Strategy = Char -> Dir

bs :: Strategy -> Range -> String -> Range
bs strategy =
  foldl'
    ( \(lo, hi) ch ->
        let mid = (lo + hi) `div` 2
         in if strategy ch == L
              then (lo, mid)
              else (mid + 1, hi)
    )

mkId str =
  let (r, c) = splitAt 7 str
      (rn, _) = bs (\ch -> if ch == 'F' then L else H) (0, 127) r
      (cn, _) = bs (\ch -> if ch == 'L' then L else H) (0, 7) c
   in rn * 8 + cn

part1 :: IO ()
part1 =
  interact $
    show . maximum . map mkId . lines

missing :: [Int] -> (Int,Int,Int,Int)
missing ids = (lo,hi,total,sum [lo .. hi] - total)
  where
    (lo, hi, total) =
      foldl'
        ( \(lo, hi, total) n ->
            if n < lo then (n, hi, total + n) else if n > hi then (lo, n, total + n) else (lo, hi, total + n)
        )
        (maxBound, minBound , 0)
        ids

part2 :: IO ()
part2 =
  interact $
    show . missing . map mkId . lines

{-# LANGUAGE FlexibleContexts #-}
module Advent.Y20.Day23 where
import           Control.Monad.Primitive     (PrimMonad, PrimState)
import qualified Data.Vector.Unboxed         as VU
import qualified Data.Vector.Unboxed.Mutable as VUM
import qualified Data.Vector.Generic as VG
import qualified Data.Vector.Generic.Mutable as VGM
import Control.Monad.ST (ST, runST)
import Control.Monad.List (foldM)
import Control.Monad (foldM_)

solve inp st n c = let rs = runST $ do
                         mv <- VUM.new (n+1)
                         let inp' = inp ++ [1 + maximum inp .. n]
                         let inits = (last inp', head inp'):zip inp' (tail inp')
                         mapM_ (uncurry $ VGM.write mv) inits
                         foldM_ (\curr _ -> step mv (minimum inp) n curr) st [1..c]
                         VU.unsafeFreeze mv
                   in rs

step :: (PrimMonad m, VGM.MVector v Int) => v (PrimState m) Int -> Int -> Int -> Int -> m Int
step mv mn mx curr = do
  n1 <- VGM.read mv curr
  n2 <- VGM.read mv n1
  n3 <- VGM.read mv n2
  n4 <- VGM.read mv n3
  let dest = destCup [n1,n2,n3] curr
  nxt <- VGM.read mv dest
  mapM_ (uncurry $ VGM.write mv) [(curr,n4),(dest,n1),(n3,nxt)]
  return n4
  where destCup nxtCups cup = let cup' = if cup == mn then mx else cup - 1 in
                                if cup' `elem` nxtCups then destCup nxtCups cup' else cup'

sampInp = [3,8,9,1,2,5,4,6,7]
inp23 = [1,9,8,7,5,3,4,6,2]

sampleSolve1 = solve sampInp 3 9 100
part1 = solve inp23 1 9 100

solve2 inp st = let rs = solve inp st 1000000 10000000
                    cp1 = rs VG.! 1
                    cp2 = rs VG.! cp1
                   in (cp1*cp2,cp1,cp2)
sampleSolve2 = solve2 sampInp 3
part2 = solve2 inp23 1

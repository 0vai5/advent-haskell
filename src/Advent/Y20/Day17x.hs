{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE BangPatterns #-}

module Advent.Y20.Day17x where

import Control.Monad.State.Strict
import Data.HashSet (HashSet)
import qualified Data.HashSet as HashSet
import Data.Hashable (Hashable)
import Data.Ix (inRange)
import GHC.Generics (Generic)
import qualified GHC.Generics
import Data.Foldable (maximumBy, minimumBy, foldl')
import Data.Ord (comparing)
import Data.List (transpose)
import Debug.Trace (trace)

data Point = Point {x::Int,y::Int,z::Int} deriving (Eq, Show, Generic)
data Bounds = Bounds {minX :: Int,maxX::Int,minY::Int,maxY::Int,minZ::Int,maxZ::Int} deriving (Show)
type Cubes = HashSet Point

instance Hashable Point

deltas = filter (/= (0,0,0)) $ (,,) <$> range <*> range <*> range where range = [-1..1]

oneCycle :: (Cubes, Bounds) -> (Cubes, Bounds)
oneCycle (cubes, Bounds {..}) =
  let points =
        [ Point x y z
          | x <- [minX .. maxX],
            y <- [minY .. maxY],
            z <- [minZ .. maxZ],
            let pt = Point x y z,
            let nbrs = activeNbrs pt,
            let active = HashSet.member pt cubes,
            (active && (2, 3) `inRange` nbrs) || (not active && nbrs == 3)
        ]
      nbrs Point{..} = (\(dx,dy,dz) -> Point (dx+x) (dy+y) (dz+z)) <$> deltas
      activeNbrs pt = trace ("Active for " ++ show pt ++ " : " ++ show rs) rs where !rs = sum . fmap (fromEnum . flip HashSet.member cubes) . nbrs $ pt
      newBnds = Bounds (minX -1) (maxX + 1) (minY -1) (maxY + 1) (minZ -1) (maxZ + 1)
   in (HashSet.fromList points, newBnds)

solve1 :: Int -> [[Char]] -> Int
solve1 n chars = HashSet.size . fst $ cycles !! n
  where
    findActive =
        map (\(i, j, k, _) -> Point i j k)
        . filter (\(_, _, _, ch) -> ch == '#')
        . mconcat
        . zipWith (\i -> zipWith (i,,0,) [0 ..]) [0 ..]
    activePoints = findActive chars
    cubes = HashSet.fromList activePoints
    bounds = let minX = subtract 1 . minimum $ fmap (\(Point x _ _) -> x) activePoints
                 minY = subtract 1 . minimum $ fmap (\(Point _ y _) -> y) activePoints
                 minZ = subtract 1 . minimum $ fmap (\(Point _ _ z) -> z) activePoints
                 maxX = (+1) . maximum $ fmap (\(Point x _ _) -> x) activePoints
                 maxY = (+1) . maximum $ fmap (\(Point _ y _) -> y) activePoints
                 maxZ = (+1) . maximum $ fmap (\(Point _ _ z) -> z) activePoints
                 in Bounds{..}
    cycles = iterate oneCycle (cubes,bounds)

-- testInput = [".#.", "..#", "###"]
-- myInput = ["#.#.##.#", "#.####.#", "...##...", "#####.##", "#....###", "##..##..", "#..####.", "#...#.#."]

{-# LANGUAGE TupleSections #-}

module Advent.Y20.Day09 where

import Commons.Utils
import Data.Array.Unboxed (Array, (!))
import qualified Data.Array.Unboxed as A
import Data.Bifunctor (bimap)
import Data.List (elemIndex)

type Numbers = Array Int Int

xmas :: Int -> Numbers -> Int
xmas prSize nums = head fails
  where
    sz = arrLength nums
    combs = [(i, j) | i <- [0 .. prSize -1], j <- [0 .. prSize -1]]
    fails = [nums ! pos | pos <- [prSize .. sz -1], check pos]
    check pos =
      let offset = pos - prSize
          n = nums ! pos
       in null [(i, j) | (i, j) <- fmap (\(m, n) -> (m + offset, n + offset)) combs, nums ! i + nums ! j == n]

part1 :: IO ()
part1 =
  interact $
    show
      . xmas 25
      . listToArray
      . fmap read
      . lines

search :: Int -> [Int] -> [Int]
search n xs = take c $ drop s xs
  where
    (s, c) = go 2 (tail xs) xs
    --it slides the first list and add it to the second list. This way in every iteration we have sum of 2 elements, then 3 elements and so on
    go g ys gsums = maybe (go (g + 1) (tail ys) gsums') (,g) (n `elemIndex` gsums')
      where
        gsums' = zipWith (+) ys gsums

solve2 :: [Int] -> Int
solve2 xs = maximum rs + minimum rs
  where
    rs = search (xmas 25 (listToArray xs)) xs

part2 :: IO ()
part2 =
  interact $
    show
      . solve2
      . fmap read
      . lines

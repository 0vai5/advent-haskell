{-# LANGUAGE RecordWildCards #-}

module Advent.Y20.Day02 where

import qualified Commons.Parsing as P
import Control.Monad (foldM)
import Data.List (foldl')
import Data.Monoid (Sum (Sum))
import Data.Semigroup (getSum)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Text.Read as TR

data Rec = Rec
  { rIx1 :: Int,
    rIx2 :: Int,
    rChar :: Char,
    rTxt :: T.Text
  }
  deriving (Show, Eq, Ord)

type Policy = Rec -> Bool

policy1 :: Policy
policy1 Rec {..} =
  rIx1 <= count && count <= rIx2
  where
    count = T.foldl' (\count elm -> if elm == rChar then count + 1 else count) 0 rTxt

policy2 :: Rec -> Bool
policy2 Rec {..} =
  ch1 /= ch2 && (ch1 == rChar || ch2 == rChar)
  where
    txt1 = T.drop (rIx1 -1) rTxt
    ch1 = T.head txt1
    ch2 = T.head $ T.drop (rIx2 - rIx1) txt1

validCount :: Policy -> [Rec] -> Int
validCount policy = getSum . foldMap (Sum . fromEnum . policy)

readRule :: T.Text -> Either String Rec
readRule txt =
  if null rs
    then Left $ "Error in parsing: " ++ T.unpack txt
    else Right $ fst $ head rs
  where
    rs = P.parse parser txt
    parser = do
      s <- P.integer
      P.char '-'
      e <- P.integer
      P.space
      ch <- P.item
      P.char ':'
      P.space
      remn <- P.many P.item
      return $ Rec s e ch (T.pack remn)

part01 :: IO ()
part01 =
  T.interact $
    T.pack . show
      . fmap (validCount policy1)
      . traverse readRule
      . T.lines

part02 :: IO ()
part02 =
  T.interact $
    T.pack . show
      . fmap (validCount policy2)
      . traverse readRule
      . T.lines

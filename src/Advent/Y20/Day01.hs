module Advent.Y20.Day01 (part1, part2) where

import Commons.Utils (parseIntegral)
import qualified Data.Text as T
import qualified Data.Text.IO as T

solve1 :: [Int] -> Int
solve1 xs = head [x * y | x <- xs, y <- xs, x + y == 2020]

solve2 :: [Int] -> Int
solve2 xs = head [x * y * z | x <- xs, y <- xs, z <- xs, x + y + z == 2020]

part1 :: IO ()
part1 =
  T.interact $
    T.pack . show . fmap solve1 . traverse parseIntegral . T.lines

part2 :: IO ()
part2 =
  T.interact $
    T.pack . show . fmap solve2 . traverse parseIntegral . T.lines

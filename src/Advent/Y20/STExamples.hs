{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE MagicHash             #-}
{-# LANGUAGE UnboxedTuples         #-}
module Advent.Y20.STExamples where

import Control.Monad.ST (runST, ST, RealWorld)
import Data.STRef (newSTRef, readSTRef, writeSTRef, modifySTRef)
import Control.Monad (forM_)
import GHC.Base (IO(IO), MutVar#, newMutVar#, readMutVar#, writeMutVar#)
import GHC.ST (ST(ST))
import Data.STRef.Lazy (STRef)

-- this runs much slower then simple sum without any st.
-- i guess that's bec it involves a lot of abstractions(thus extra function invocations) like modifySTRef etc
sumST :: Num a => [a] -> a
sumST xs = runST $ do           -- runST takes out stateful code and makes it pure again.
    n <- newSTRef 0             -- Create an STRef (place in memory to store values)
    forM_ xs $ \x -> do         -- For each element of xs ..
        modifySTRef n (+x)      -- add it to what we have in n.
    readSTRef n                 -- read the value of n, and return it.

-- xxx = let
--   nv :: ST s (STRef s Int)
--   nv = newSTRef 0
--   -- rv = readSTRef nv
--   v = runST nv in runST v

-- here too normal fib is faster. tested on 2000000 took time 13s for normal fib and 16s for this
-- note in both using strict evaluation while adding numbers
fibST :: Integer -> Integer
fibST n =
    if n < 2
    then n
    else runST $ do
        x <- newSTRef 0
        y <- newSTRef 1
        fibST' n x y
    where fibST' 0 x _ = readSTRef x
          fibST' n x !y = do
              x' <- readSTRef x
              y' <- readSTRef y
              writeSTRef x y'
              writeSTRef y $! x'+y'
              fibST' (n-1) x y

fib :: Integer -> Integer
fib n | n < 2 = n
      | otherwise = fib (n-1) + fib (n-2)

fib' :: Integer -> Integer
fib' 0 = 0
fib' n = fib' 0 0 1 where
  fib' (!c) (!x) (!y) | c == n = y
             | otherwise = fib' (c+1) y (x+y)


-- creating my own ST


-- IO a and ST RealWorld a

io2st :: IO a -> ST RealWorld a
io2st (IO a) = ST a

st2io :: ST RealWorld a -> IO a
st2io (ST a) = IO a

-- data MutVar# s a Source

-- A MutVar# behaves like a single-element mutable array.

-- newMutVar# :: a -> State# s -> (#State# s, MutVar# s a#) Source

-- Create MutVar# with specified initial value in specified state thread.

-- readMutVar# :: MutVar# s a -> State# s -> (#State# s, a#) Source

-- Read contents of MutVar#. Result is not yet evaluated.

-- writeMutVar# :: MutVar# s a -> a -> State# s -> State# s Source

-- Write contents of MutVar#.

-- sameMutVar# :: MutVar# s a -> MutVar# s a -> Int# Source

-- atomicModifyMutVar# :: MutVar# s a -> (a -> b) -> State# s -> (#State# s, c#) Source

-- casMutVar# :: MutVar# s a -> a -> a -> State# s -> (#State# s, Int#, a#) Source

data MyRef s a = MyRef (MutVar# s a)

newMyRef :: a -> ST s (MyRef s a)
newMyRef a = ST $ \s -> case newMutVar# a s of (# new_s, mv #) -> (# new_s, MyRef mv #)

readMyRef :: MyRef s a -> ST s a
readMyRef (MyRef mv)  = ST $ \s -> readMutVar# mv s

writeMyRef :: MyRef s a -> a -> ST s ()
writeMyRef (MyRef mv) val = ST $ \s -> (# writeMutVar# mv val s, () #)

mysumST :: Num a => [a] -> a
mysumST xs = runST $ do           -- runST takes out stateful code and makes it pure again.
    n <- newMyRef 0             -- Create an STRef (place in memory to store values)
    forM_ xs $ \x -> do
        val <- readMyRef n
        writeMyRef n (val + x)
    readMyRef n                 -- read the value of n, and return it.

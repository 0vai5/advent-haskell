{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Advent.Y20.Day07Again where

-- taken from https://notes.abhinavsarkar.net/2020/aoc-wk2

-- First, a parser framework from (almost) scratch:
import Control.Monad.State.Strict (MonadState(..), StateT, runStateT, modify)
import Control.Monad.Trans.Maybe (MaybeT(..))
import Control.Monad.Identity (Identity (..), runIdentity)
import Control.Applicative (Alternative(..), optional)

--other imports (non-parser)
import Data.Char (digitToInt, isDigit, isAlpha)
import Data.Maybe (fromMaybe)
import Data.Graph.Wrapper (fromListSimple, transpose, reachableVertices)
import Data.Functor (($>))

newtype Parser i o = Parser { runParser_ :: StateT i (MaybeT Identity) o }
  deriving (Functor, Applicative, Alternative, Monad , MonadState i)

runParser input =
  fmap fst . runIdentity . runMaybeT . flip runStateT input . runParser_

satisfy :: (a -> Bool) -> Parser [a] a
satisfy pr = get >>= \case { (c:cs) | pr c -> put cs >> return c; _ -> empty }

lookahead :: Parser [a] (Maybe a)
lookahead = get >>= \case { [] -> return Nothing; (c:_) -> return (Just c) }

consume :: Parser [a] ()
consume = modify tail

char c = satisfy (== c)

string = \case { "" -> pure ""; (c:cs) -> (:) <$> char c <*> string cs }

digit = digitToInt <$> satisfy isDigit

num = foldl1 (\n d -> n * 10 + d) <$> some digit

space = char ' '

word = some $ satisfy isAlpha

separatedBy v s = (:) <$> v <*> many (s *> v) <|> pure []

-- Next, use the parser framework to parse the input:
color = (++) <$> (word <* space) <*> word
bags = string " bag" <* optional (char 's')
container = color <* bags <* string " contain "

contained = string "no other bags" $> []
  <|> ((,) <$> num <*> (space *> color <* bags)) `separatedBy` string ", "

rule = (,) <$> container <*> contained <* char '.'
rules = rule `separatedBy` char '\n'

solveDay7 :: IO ()
solveDay7 = do
  Just bagRules <- flip runParser rules <$> readFile "inputs/2020/input07.txt"
  -- Finally, solve the problem:
  let colorDeps = transpose . fromListSimple $ map (fmap $ map snd) bagRules
  print $ length (reachableVertices colorDeps "shinygold") - 1 -- part 1

  let bagCount color =
        let colors = fromMaybe [] $ lookup color bagRules
        in sum (map fst colors) + sum (map (\(c, col) -> c * bagCount col) colors)

  print $ bagCount "shinygold" -- part 2

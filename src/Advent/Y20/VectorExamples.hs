{-# LANGUAGE RankNTypes #-}

module Advent.Y20.VectorExamples where

-- import           Data.Vector.Unboxed ((!), (//))
-- import qualified Data.Vector.Unboxed as V
-- import           System.Random       (randomRIO)

-- main :: IO ()
-- main = do
--     let v0 = V.replicate 10 (0 :: Int)

--         loop v 0 = return v
--         loop v rest = do
--             i <- randomRIO (0, 9)
--             let oldCount = v ! i
--                 v' = v // [(i, oldCount + 1)]
--             loop v' (rest - 1)

--     vector <- loop v0 (10^6)
--     print vector

-- import           Control.Monad               (replicateM_)
-- import           Data.Vector.Unboxed         (freeze)
-- import qualified Data.Vector.Unboxed.Mutable as V
-- import           System.Random               (randomRIO)

-- main :: IO ()
-- main = do
--     vector <- V.replicate 10 (0 :: Int)

--     replicateM_ (10^6) $ do
--         i <- randomRIO (0, 9)
--         oldCount <- V.read vector i
--         V.write vector i (oldCount + 1)

--     ivector <- freeze vector
--     print ivector

-- import           Data.Vector.Unboxed ((!), (//))
-- import qualified Data.Vector.Unboxed as V
-- import           System.Random       (randomRIO)

-- main :: IO ()
-- main = do
--     let v0 = V.replicate 10 (0 :: Int)

--         loop v 0 = return v
--         loop v rest = do
--             i <- randomRIO (0, 9)
--             let oldCount = v ! i
--                 v' = v // [(i, oldCount + 1)]
--             loop v' (rest - 1)

--     vector <- loop v0 (10^6)
--     print vector

-- import           Control.Monad               (replicateM_)
-- import           Data.Vector.Unboxed         (freeze)
-- import qualified Data.Vector.Unboxed.Mutable as V
-- import           System.Random               (randomRIO)

-- main :: IO ()
-- main = do
--     vector <- V.replicate 10 (0 :: Int)

--     replicateM_ (10^6) $ do
--         i <- randomRIO (0, 9)
--         oldCount <- V.read vector i
--         V.write vector i (oldCount + 1)

--     ivector <- freeze vector
--     print ivector


-- import           Data.Vector.Unboxed         (freeze, unsafeFreeze)
-- import qualified Data.Vector.Unboxed.Mutable as V

-- testingUnsafeFreezeVector :: IO ()
-- testingUnsafeFreezeVector = do
--     vector <- V.replicate 1 (0 :: Int)
--     V.write vector 0 1
--     ivector <- unsafeFreeze vector
--     --ivector <- freeze vector
--     print ivector
--     V.write vector 0 2
--     print ivector

import           Control.Monad.Primitive     (PrimMonad, PrimState)
import qualified Data.Vector.Unboxed         as VU
import qualified Data.Vector.Unboxed.Mutable as VUM
import qualified Data.Vector.Generic as VG
import qualified Data.Vector.Generic.Mutable as VGM
import           System.Random               (StdGen, getStdGen, randomR)
import Control.Monad.ST (ST, runST)


myModify :: VU.Unbox a => (forall s. VUM.MVector s a -> ST s ()) -> VU.Vector a -> VU.Vector a
myModify f v = runST $ VU.thaw v >>= \mv -> f mv >> VU.unsafeFreeze mv

shuffleM :: (PrimMonad m, VU.Unbox a)
         => StdGen
         -> Int -- ^ count to shuffle
         -> VUM.MVector (PrimState m) a
         -> m ()
shuffleM _ i _ | i <= 1 = return ()
shuffleM gen i v = do
    VUM.swap v i' index
    shuffleM gen' i' v
  where
    (index, gen') = randomR (0, i') gen
    i' = i - 1

shuffle :: VU.Unbox a
        => StdGen
        -> VU.Vector a
        -> VU.Vector a
shuffle gen vector = VU.modify (shuffleM gen (VU.length vector)) vector


shuffleM' :: (PrimMonad m, VGM.MVector v a)
         => StdGen
         -> Int -- ^ count to shuffle
         -> v (PrimState m) a
         -> m ()
shuffleM' _ i _ | i <= 1 = return ()
shuffleM' gen i v = do
    VGM.swap v i' index
    shuffleM' gen' i' v
  where
    (index, gen') = randomR (0, i') gen
    i' = i - 1

shuffle' :: VG.Vector v a
        => StdGen
        -> v a
        -> v a
shuffle' gen vector = VG.modify (shuffleM' gen (VG.length vector)) vector

myShuffle :: IO ()
myShuffle = do
    gen <- getStdGen
    print $ shuffle' gen $ VU.enumFromTo 1 (20 :: Int)


-- import           Data.Vector.Algorithms.Merge (sort)
-- import qualified Data.Vector.Generic.Mutable  as M
-- import qualified Data.Vector.Unboxed          as V
-- import           System.Random                (randomRIO)

-- main :: IO ()
-- main = do
--     vector <- M.replicateM 100 $ randomRIO (0, 999 :: Int)
--     sort vector
--     V.unsafeFreeze vector >>= print

-- {-# LANGUAGE FlexibleContexts #-}
-- import qualified Data.Vector as VB
-- import qualified Data.Vector.Storable as VS
-- import qualified Data.Vector.Unboxed as VU

-- testVectorLaziness :: IO ()
-- testVectorLaziness = do
--   print $ VB.head $ VB.fromList (():undefined)
  -- print $ VS.head $ VS.fromList (():undefined)
  -- print $ VU.head $ VU.fromList (():undefined)

  -- print $ VB.head $ VB.fromList [(), undefined]
  -- print $ VS.head $ VS.fromList [(), undefined]
  -- print $ VU.head $ VU.fromList [(), undefined]

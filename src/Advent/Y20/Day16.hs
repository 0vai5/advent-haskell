{-# LANGUAGE RecordWildCards #-}
module Advent.Y20.Day16 where

import qualified Commons.Parsing as P
import qualified Data.Text as T
import qualified Data.Vector.Generic as VG
import qualified Data.Vector as VB
import Data.Foldable (foldl')
import Data.List (isPrefixOf)
import Debug.Trace
import Data.Ix (inRange)

data Rule = Rule {
  name :: String,
  ranges :: [(Int,Int)]} deriving (Show)

data Input = Input {
  rules :: [Rule],
  yourTicket :: [Int],
  nearByTickets :: [[Int]]} deriving (Show)

parser :: P.Parser Input
parser = Input <$> rules <* P.space <*> yourTicket <* P.space <*> nearByTickets where
  ticket = P.separatedBy P.int (P.char ',')
  interval = (,) <$> P.nat <*> (P.char '-' *> P.nat)
  intervals = P.separatedBy interval (P.text " or ")
  key = P.some (P.sat (/= ':')) <* P.char ':'
  rule = Rule <$> key <*> (P.space *> intervals)
  rules = P.separatedBy rule P.space
  yourTicket = P.text "your ticket:" *> (P.space *> ticket)
  nearByTickets = P.text "nearby tickets:" *> (P.space *> P.separatedBy ticket P.space)

solve1 :: Input -> Int
solve1 Input{..} = sum rs where
  rs = [n | ticket <- nearByTickets, n <- ticket, check n rules]
  check n = all (\(Rule _ intervals) -> all (\(s,e) -> not $ s <= n && n <= e) intervals)

part1 :: IO ()
part1 = interact $ show . solve1 . parseIt where
        parseIt = fst . head . P.parse parser . T.pack

solve2 :: String -> Input -> [Int]
solve2 prefix Input{..} = rs where
  tickets = filter initialCheck nearByTickets
  initialCheck = all (\n -> any (\Rule{..} -> any (`inRange` n) ranges)  rules)
  match :: [Int] -> [[(Int,Rule)]] -> [[Rule]]
  match [] accum = fmap (fmap snd . reverse) accum
  match (psn:psns) accum = match psns accum' where
    accum' = [newRule:rules |
              newRule@(rulePsn, _) <- rulesSatisfyingAt psn,
              rules <- accum,
              rulePsn `notElem` fmap fst rules]
  rulesSatisfyingAt psn = [r | r@(i,Rule{..}) <- zip [0..] rules, all (\n -> any (`inRange` n) ranges) ns] where
    ns = map (!! psn) tickets
  matchedRules = match [0..length rules-1] [[]]
  rs = fmap (foldl mltply 1 . zip yourTicket) matchedRules where
    mltply r (n,Rule name _) = if prefix `isPrefixOf` name then r*n else r

part2 :: IO ()
part2 = interact $ show . solve2 "departure" . parseIt where
    parseIt = fst . head . P.parse parser . T.pack

{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}

module Advent.Y20.Day20 where

import Commons.Algos (combinations)
import qualified Commons.Parsing as P
import Data.Bifunctor (bimap)
import qualified Data.Bifunctor
import Data.Bits (Bits (bitSizeMaybe, clearBit, setBit, testBit, (.&.), (.|.)), zeroBits)
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HM
import Data.Hashable (Hashable)
import Data.IntMap (IntMap)
import qualified Data.IntMap as IM
import Data.List (foldl', transpose)
import Data.Map (Map)
import qualified Data.Map as M
import Data.Maybe (fromMaybe)
import Data.Set (Set)
import qualified Data.Set as S
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Vector.Generic as VG
import qualified Data.Vector.Unboxed as VU
import qualified Debug.Trace as Debug
import GHC.Generics (Generic)
import Numeric (readInt)

data Side = L | R | T | B | L' | R' | T' | B' deriving (Eq, Show, Enum, Bounded, Generic)

instance Hashable Side

type Sides = VU.Vector Int

data Tile = Tile
  { tno :: Int,
    chars :: [String],
    sides :: Sides
  }
  deriving (Show, Eq)

type TilesTable = IntMap Tile

parser :: P.Parser TilesTable
parser = IM.fromList . fmap (\tl -> (tno tl, tl)) <$> tiles
  where
    f :: [String] -> Sides
    f chars = VU.fromList $ mkInt . flip mkSide chars <$> [minBound .. maxBound]
    mkInt s = fst . head $ readInt 2 (const True) (fromEnum . (== '#')) s
    mkSide L = map head
    mkSide R = map last
    mkSide T = head
    mkSide B = last
    mkSide L' = reverse . mkSide L
    mkSide R' = reverse . mkSide R
    mkSide T' = reverse . mkSide T
    mkSide B' = reverse . mkSide B
    tnum = P.text "Tile " *> P.int <* P.char ':' <* P.newLine
    tuple = (,) <$> tnum <*> P.separatedBy P.nonSpace P.newLine
    tile = fmap (\(tno, chars) -> Tile tno chars $ f chars) tuple
    tiles = P.separatedBy tile (P.newLine <* P.newLine)

data Match = Match
  { ltno :: Int, --tile to match or lhs
    lside :: Side,
    rtno :: Int, --tile matched or rhs
    rside :: Side
  }
  deriving (Show)

data Adjust = CW | VFLP | HFLP deriving (Show, Eq, Generic)

instance Hashable Adjust

type Position = (Int, Int)

type SeaMap = Map Position Tile

transformTable :: HashMap Adjust (IntMap Int)
transformTable =
  HM.fromList
    [ (CW, IM.fromList $ map (\v -> (fromEnum (cwSideMap HM.! v), fromEnum v)) [L, T, R, B, L', T', R', B']),
      (VFLP, IM.fromList $ zipWith (\v rv -> (fromEnum v, fromEnum rv)) [L, T, R, B, L', T', R', B'] [R, T', L, B', R', T, L', B]),
      (HFLP, IM.fromList $ zipWith (\v rv -> (fromEnum v, fromEnum rv)) [L, T, R, B, L', T', R', B'] [L', B, R', T, L, B', R, T'])
    ]

transformChars CW = fmap reverse . transpose
transformChars VFLP = fmap reverse
transformChars HFLP = reverse

transformTile :: Adjust -> Tile -> Tile
transformTile adjust Tile {..} = Tile {tno = tno, chars = transformChars adjust chars, sides = transformSides adjust sides}
  where
    transformSides adjust sides =
      let oprMap = transformTable HM.! adjust
       in VU.fromList . map ((sides VU.!) . (oprMap IM.!) . fromEnum) $ [(minBound :: Side) ..]

pssblts = (,) <$> [L, T, R, B] <*> [minBound ..]

matchTile :: Tile -> TilesTable -> [Match]
matchTile t1 tt = rs
  where
    get side Tile {..} = sides VU.! fromEnum side
    rs = [Match (tno t1) sd1 (tno t2) sd2 | t2 <- IM.elems tt, (sd1, sd2) <- pssblts, get sd1 t1 == get sd2 t2]

buildMap :: TilesTable -> SeaMap
buildMap tt = bfs [(stTl, stPos)] remnTiles (M.singleton stPos stTl)
  where
    Just (stTl, remnTiles) = IM.minView tt
    stPos = (0, 0)
    bfs :: [(Tile, Position)] -> TilesTable -> SeaMap -> SeaMap
    bfs [] _ seaMap = seaMap
    bfs ((tl, tlPos) : inProgress) unvisited seaMap = bfs (inProgress ++ currentVisits) newUnvisited newSeaMap
      where
        currentVisits = map (processMatch tlPos) (matchTile tl unvisited)
        newUnvisited = foldl' (\rs (tl, _) -> IM.delete (tno tl) rs) unvisited currentVisits
        newSeaMap = foldl' insertMatch seaMap currentVisits
        insertMatch rs (matchedTile, newPos) = M.insertWithKey f newPos matchedTile rs
        f = \k old new -> error $ "Key already exist: " ++ show k ++ ", old: " ++ show (tno old) ++ show (tno new)
        coord (lx, ly) L = (lx -1, ly)
        coord (lx, ly) R = (lx + 1, ly)
        coord (lx, ly) T = (lx, ly -1)
        coord (lx, ly) B = (lx, ly + 1)
        coord _ sd = error $ "Invalid coord" ++ show sd
        processMatch lPos Match {..} = (foldl' (flip transformTile) rtile adjusts, coord lPos lside)
          where
            rtile = unvisited IM.! rtno
            adjusts = findAdjusts (lside, rside)

cwSideMap = HM.fromList [(L, T'), (L', T), (T, R), (T', R'), (R, B'), (R', B), (B, L), (B', L')]

findAdjusts pair@(fixed, toFix)
  | pair `elem` [(L, R), (R, L), (T, B), (B, T)] = []
  | pair `elem` [(L, R'), (R, L')] = [HFLP]
  | pair `elem` [(T, B'), (B, T')] = [VFLP]
  | otherwise = CW : findAdjusts (fixed, cwSideMap HM.! toFix)

dims seaMap =
  let ((mnX, mnY), _) = M.findMin seaMap
      ((mxX, mxY), _) = M.findMax seaMap
   in (mnX, mnY, mxX, mxY)

part1 =
  T.interact $
    T.pack . show . product . cornerTiles . buildMap . fst . head . P.parse parser
  where
    cornerTiles seaMap = map (tno . (seaMap M.!)) [(mnX, mnY), (mnX, mxY), (mxX, mnY), (mxX, mxY)]
      where
        (mnX, mnY, mxX, mxY) = dims seaMap

joinTiles :: SeaMap -> [[Char]]
joinTiles seaMap = foldl' mergeCols emptyCol [mnX .. mxX]
  where
    (mnX, mnY, mxX, mxY) = dims seaMap
    removeBoundary = fmap (tail . init) . tail . init
    emptyCol = repeat []
    mergeCols cols x = zipWith (++) cols $ foldl' (appendTileInCol x) [] [mnY .. mxY]
    appendTileInCol x col y = col ++ removeBoundary cell
      where
        cell = chars $ seaMap M.! (x, y)

monster = ["                  # ", "#    ##    ##    ###", " #  #  #  #  #  #   "]

waterRoughness :: SeaMap -> Int
waterRoughness seaMap = S.size $ setWithMonsters `S.difference` foundMonsters
  where
    setWithMonsters = head $ filter (not . null . searchMonstersInSet) allPossibilities
    foundMonsters = S.unions $ searchMonstersInSet setWithMonsters
    allPossibilities = map doTransforms allTransforms
      where
        doTransforms = mkSet . foldl' (flip transformChars) joinedChars
        allTransforms = cwTransforms ++ map (VFLP :) cwTransforms where cwTransforms = [[], [CW], [CW, CW], [CW, CW, CW]]
    searchMonstersInSet set = filter (`S.isSubsetOf` set) monsters
      where
        monsters = fmap (\(x, y) -> S.map (bimap (x +) (y +)) monsterSet) coords
        (setX, setY) = S.findMax set
        coords = (,) <$> [0 .. (setX - monsterX)] <*> [0 .. (setY - monsterY)]
    mkSet chars = S.fromList [(i, j) | (i, row) <- zip [0 ..] chars, (j, c) <- zip [0 ..] row, c == '#']
    joinedChars = joinTiles seaMap
    monsterSet = mkSet monster
    (monsterX, monsterY) = S.findMax monsterSet

part2 =
  T.interact $
    T.pack . show . waterRoughness . buildMap . fst . head . P.parse parser

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE BangPatterns #-}
module Advent.Y20.Day15 where

import Control.Monad.Primitive (PrimMonad (PrimState))
import Data.Vector.Generic.Mutable (MVector)
import qualified Data.Vector.Generic.Mutable as VGM
import qualified Data.Vector.Unboxed.Mutable as VUM
import Control.Monad.ST.Strict(runST, ST)
import Control.Monad (foldM_, zipWithM_, foldM)
import Data.List.Split (splitOn)
import Data.Foldable (foldlM)

play :: (PrimMonad m, MVector v Int) => v (PrimState m) Int -> Int -> Int -> m Int
play mv n turn =
  VGM.read mv n >>=
    \lastTurn -> VGM.write mv n turn >> return (if lastTurn == 0 then 0 else turn - lastTurn)

game :: Int -> [Int] -> Int
game steps nums = rs where
  sz = maximum $ steps:(succ <$> nums)
  l = length nums
  rs = runST $
    VUM.new sz >>= \mv -> zipWithM_ (VGM.write mv) (init nums) [1..] >> foldlM (play mv) (last nums) [l..steps-1]

part1 :: IO ()
part1 =
  interact $ show . game 2020 . fmap read . splitOn ","

part2 :: IO ()
part2 =
  interact $ show . game 30000000 . fmap read . splitOn ","

day15a :: IO ()
day15a = print $ game 2020 [18,8,0,5,4,1,20]

day15b :: IO ()
day15b = print $ game 30000000 [18,8,0,5,4,1,20]

-- import Data.IntMap.Strict as IntMap (empty, insert, IntMap, fromList, lookup)
-- import Data.List.Split (splitOn)

-- game :: Int -> [Int] -> [Int]
-- game c ns = go (c-(sz-1)) mem sz (last ns) (reverse except1) where
--   --assuming starting numbers ns do not have duplicates
--   sz = length ns
--   except1 = init ns
--   mem = IntMap.fromList $ zip except1 [1..]
--   go c _ _ _ (!accum) | c <= 0 = drop (-c) accum
--   go c mem turn (!last) (!accum) = go (c-1) (IntMap.insert last turn mem) (turn+1) new (last:accum) where
--     new = maybe 0 (turn -) (IntMap.lookup last mem)

-- part1 :: IO ()
-- part1 =
--   interact $ show . head . game 2020 . fmap read . splitOn ","

-- part2 :: IO ()
-- part2 =
--   interact $ show . head . game 30000000 .fmap read . splitOn ","

-- day15a :: IO ()
-- day15a = print $ show . head . game 2020 $ [18,8,0,5,4,1,20]

-- day15b :: IO ()
-- day15b = print $ show . head . game 30000000 $ [18,8,0,5,4,1,20]

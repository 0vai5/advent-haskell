module Advent.Y20.Day13 where

import Commons.Utils (parseIntegral)
import Data.Text(Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Data.Foldable (find, maximumBy)
import Data.List ((\\))
import Data.Ord (comparing)

parse :: Text -> Either String (Int,[Int])
parse txt = (,) <$> parseIntegral first <*> traverse parseIntegral (filter (/="x") $ T.splitOn "," second)
  where [first,second] = T.lines txt

solve1 st = uncurry (*) . minimum . fmap (\id -> ((-st) `mod` id, id))

part1 :: IO ()
part1 =
  T.interact $
    T.pack . show . fmap (uncurry solve1) . parse

parse' :: Text -> Either String [Int]
parse' txt = traverse (parseIntegral . (\s -> if s == "x" then "1" else s)) (T.splitOn "," lns)
  where lns = head $ T.lines txt

solve2 :: [Int] -> Int
solve2 ns = go (head ns) (head ns) (tail ns) - (length ns - 1) where
  go _ tsstmp [] = tsstmp
  go lcm tsstmp (1:ns') = go lcm (tsstmp+1) ns'
  go lcm tsstmp (n:ns') = go lcm' tsstmp' ns'
    where lcm' = lcm * n
          tsstmp' = head [r | mltpl <- [lcm,2*lcm..], let r = tsstmp + 1 + mltpl, r `mod` n == 0]

part2 :: IO ()
part2 =
  T.interact $
    T.pack . show . fmap solve2 . parse'

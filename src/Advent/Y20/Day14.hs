{-# LANGUAGE RecordWildCards #-}
module Advent.Y20.Day14 where

import Data.Bits (Bits((.&.), (.|.), shiftL, shiftR, popCount, xor, bitSizeMaybe, testBit, clearBit, setBit), zeroBits, complement)
import Data.Map as Map (empty, insert, Map)
import Data.List (foldl', (\\))
import Data.String (IsString)
import Data.Text (Text)
import Data.Void (Void)
import Data.Word (Word64)
import Numeric (readInt)
import qualified Commons.Parsing as P
import qualified Data.Text as T
import qualified Data.Text.IO as T

-- reimplemented the solution from https://github.com/ephemient/aoc2020/blob/main/hs/src/Day14.hs
-- i came up independently with this idea of andMask and orMask before looking at the solution but they too came up with the same idea!
-- ofcourse their implementation is much elegant then what i could have come up!

data Instruction a b c
  = Mask { andMask :: a, orMask :: a }
  | Write { writeAddr :: b, writeValue :: c }

parser :: (Num a, Num b, Num c, Read a, Read b, Read c) => P.Parser [Instruction a b c]
parser = P.some (parsem P.<|> parsew) where
  chars = P.some $ P.char 'X' P.<|> P.char '0' P.<|> P.char '1'
  mask s = let (orMask,""):_ = readInt 2 (const True) (fromEnum . (== '1')) s
               (andMask,""):_ = readInt 2 (const True) (fromEnum . (/= '0')) s in
               Mask {..}
  parsew = Write <$> (P.text "mem" >> P.char '[' >> P.num <* P.char ']') <*> (P.text " = " *> P.number)
  parsem = P.text "mask" >> P.text " = " >> (mask <$> chars <* P.space)

solve1 :: [Instruction Word64 Word64 Word64] -> Word64
solve1 = sum . fst . foldl' f (Map.empty, (complement zeroBits, zeroBits)) where
  f (map,_) Mask{..} = (map,(andMask,orMask))
  f (map,(andMask,orMask)) Write{..} = (Map.insert writeAddr ((writeValue  .|. orMask) .&. andMask) map, (andMask,orMask))

part1 :: IO ()
part1 =
  T.interact $
    T.pack . show . fst . head . P.parse (fmap solve1 parser)

solve2 :: [Instruction Word64 Word64 Word64] -> Word64
solve2 = sum . fst . foldl' f (Map.empty, [(oneBits, zeroBits)]) where
  oneBits = complement zeroBits
  f (map,_) Mask{..} = (map,fmap (\(andMask', orMask') -> (andMask', orMask' .|. orMask)) (floatings (andMask `xor` orMask)))
  f (map,masks) Write{..} = (foldr (\(andMask,orMask) map' -> Map.insert ((writeAddr .|. orMask) .&. andMask) writeValue map') map masks, masks)

floatings :: Word64 -> [(Word64,Word64)]
floatings n = [ (foldl' clearBit oneBits ands, foldl' setBit zeroBits ors) | ors <- powerSet ones, let ands = ones \\ ors] where
  (Just sz) = bitSizeMaybe n
  ones = [i | i <- [0..sz-1], testBit n i]
  oneBits = complement zeroBits

powerSet :: [Int] -> [[Int]]
powerSet [] = [[]]
powerSet (x:xs) = ps ++ fmap (x:) ps where ps = powerSet xs

part2 :: IO ()
part2 =
  T.interact $
    T.pack . show . fst . head . P.parse (fmap solve2 parser)

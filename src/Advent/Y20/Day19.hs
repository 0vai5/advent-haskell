module Advent.Y20.Day19 where

import qualified Text.ParserCombinators.ReadP as RP
import qualified Data.Text as T
import Data.IntMap (IntMap)
import qualified Data.IntMap as M
import Data.Maybe (fromMaybe, fromJust)
import qualified Commons.Parsing as P
import Control.Applicative ((<|>))
import Control.Monad (void)
import Data.List (foldl1')

data Expr = C Char | SOP [[Int]] deriving (Show)

-- parsing input to create expression tree using toy parser
inputParser :: P.Parser (IntMap Expr,[String])
inputParser = (,) <$> pexpr <* P.newLine <*> pstrs where
  pkey = P.int <* P.char ':' <* P.space
  pchar = fmap C $ pquote *> P.item <* pquote  where pquote = P.char '\"'
  pints = SOP <$> P.separatedBy (P.separatedBy P.int $ P.char ' ') (P.symbol "|")
  pexpr = fmap M.fromList $ P.some $ (,) <$> pkey <*> (pchar <|> pints) <* P.newLine
  pstrs = P.some $ P.nonSpace <* P.newLine

--notice no interest in the result but only consuming the result
mkParser :: IntMap Expr -> Int -> RP.ReadP ()
mkParser table id = case fromJust $ M.lookup id table of
  (C ch) -> void $ RP.char ch
  (SOP ints) -> foldl1' (<|>) . fmap (foldl1' (>>) . fmap (mkParser table)) $ ints

match :: RP.ReadP () -> String -> Bool
match parser = not . null . RP.readP_to_S parser

part :: (T.Text -> T.Text) -> IO ()
part changeInput = interact $ show . uncurry solve . parseIt where
    parseIt = fst . head . P.parse inputParser . changeInput . T.pack
    solve table = sum . fmap (fromEnum . match parser) where parser = mkParser table 0 *> RP.eof

part1 = part id
part2 = part $ T.replace "8: 42" "8: 42 | 42 8"
                . T.replace "11: 42 31" "11: 42 31 | 42 11 31"

-- this is old brain fart i did but part 2 made me realise to instead
-- write generate the parser from the input(came up myself)  and current
-- toy parser i have been using all along is not right here. Why? Bec this parser is greedy and does not account for multiple
-- possibilities. Eg some (charParser) "ab" can have two answers [("a","b")] and [("ab", "")]. The toy parser is greedy and consumes
-- as much as it can. Thus toy parser will return [("ab","")] only.

-- extrapolate :: Int -> IntMap Expr -> HashSet String
-- extrapolate n mem = S.fromList $ go n where
--   go i = fromMaybe [] $ M.lookup i mem >>= \case
--     C ch -> Just [[ch]]
--     SOP ors -> Just [s | ands <- ors, s <- foldl' f [[]] ands] where
--       f rs i = go i >>= (\s -> ( ++ s) <$> rs)

-- part1 :: IO ()
-- part1 = interact $ show . uncurry solve . parseIt where
--     parseIt = fst . head . P.parse inputParser . T.pack
--     solve mem = sum . fmap (fromEnum . (`S.member` set)) where set = extrapolate 0 mem

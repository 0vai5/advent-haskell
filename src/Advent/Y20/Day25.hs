module Advent.Y20.Day25 where

import Data.List (elemIndex)
import Commons.Utils(foldTerminate)

gen sn (i,n) = (i+1,(sn * n) `mod` 20201227)

findLoop k = until (\(i,n) -> n == k) (gen 7) (0,1)

solve k1 k2 = rs1 where
  (c1,_) = findLoop k1
  (_,rs1) = until (\(i,n) -> i == c1) (gen k2) (0,1)

sample = solve 5764801 17807724

cardKey = 18499292
doorKey = 8790390
part1 = solve cardKey doorKey

{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE BangPatterns #-}
module Advent.Y20.Day10 where

import Data.List (sort)
import Data.Foldable (foldl')
import Data.List.NonEmpty (partition, NonEmpty ((:|)))

solve1 :: [Int] -> (Int,Int,Int,[Int])
solve1 xs = (ones*threes,ones,threes,rs) where
  ys = 0:sort xs
  rs = sort $ zipWith (-) (tail ys) ys
  ones = length $ takeWhile (==1) rs
  threes = 1 + (length.dropWhile (/=3) $ rs)

-- solve2 :: [Int] -> Int
-- solve2 xs = length result where
--   result = combs diffs [[]]
--   ys = 0:sort xs ++ [maximum xs + 3]
--   diffs = zipWith (-) (tail ys) ys

-- combs :: (Eq a, Num a, Ord a) => [a] -> [[a]] -> [[a]]
-- combs [] = id
-- combs (e:es) = combs es . foldl' (\rs -> \case
--         (c:cs) | c+e <= 3 -> (e:c:cs):(c+e:cs):rs
--                | otherwise -> (e:c:cs):rs
--         [] -> [e]:rs) []


solve2' :: [Int] -> Int
solve2' xs = mk diffs where
  ys = 0:sort xs ++ [maximum xs + 3]
  diffs = zipWith (-) (tail ys) ys

mk :: [Int] -> Int
mk xs = case span (< 3) xs of
  ([],3:ys) -> mk ys
  ([],ys) -> 1 --ys must be empty here
  (x:xs,ys) -> combs' xs [x] * mk ys

-- since only the last element is important, in the second argument i am using [Int] instead of [[Int]] see the commented code for original version
combs' :: [Int] -> [Int] -> Int
combs' [] = length
combs' (e:es) = combs' es . foldl' (\rs n -> if e+n <= 3 then e:e+n:rs else e:rs) []

part1 :: IO ()
part1 =
  interact $
    show
    . solve1
    . fmap read
    . lines

part2 :: IO ()
part2 =
  interact $
    show
    . solve2'
    . fmap read
    . lines

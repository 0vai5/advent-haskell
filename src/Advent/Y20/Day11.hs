{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TupleSections #-}

module Advent.Y20.Day11 where

import Control.Monad (join)
import Data.Array.Base (IArray (unsafeAt))
import Data.Array.Unboxed
import qualified Data.Functor
import Data.List (find)
import Data.Maybe (listToMaybe, mapMaybe)
import Data.Monoid (Sum (Sum))
import GHC.Arr (unsafeIndex)

-- layout fits neatly on a grid. Each position is either floor (.), an empty seat (L), or an occupied seat (#).

type Grid = UArray (Int, Int) Char

elemAt :: Grid -> (Int, Int) -> Maybe Char
elemAt grid ix =
  if bds `inRange` ix
    then Just $ grid `unsafeAt` (bds `unsafeIndex` ix)
    else Nothing
  where
    bds = bounds grid

loop :: Bool -> Int -> Grid -> Grid
loop far d grid = if null cells then grid else loop far d $ grid // cells
  where
    bnds = bounds grid
    cells =
      indices grid >>= \ix -> case (grid ! ix, nbrs ix) of
        ('.', _) -> []
        ('L', adjs) | Just '#' `notElem` adjs -> [(ix, '#')]
        ('#', adjs) | not . null . drop d . filter (== Just '#') $ adjs -> [(ix, 'L')]
        _ -> []
    nbrs ix@(r, c) = los ix <$> deltas
    deltas = filter (/= (0, 0)) $ (,) <$> [-1 .. 1] <*> [-1 .. 1]
    los (r, c) (dr, dc) =
      join . find (/= Just '.')
        . takeWhile (not . null)
        . fmap (grid `elemAt`)
        . limitVision
        $ zip [r + dr, r + 2 * dr ..] [c + dc, c + 2 * dc ..]
    limitVision
      | far = id
      | otherwise = take 1

parse :: String -> Grid
parse input = array ((0, 0), (r -1, c -1)) cells
  where
    rows = lines input
    r = length rows
    c = maximum . fmap length $ rows
    cells = zip [0 ..] rows >>= (\(i, row) -> (\(j, ch) -> ((i, j), ch)) <$> zip [0 ..] row)

countOccupied :: Grid -> Sum Int
countOccupied arr = foldMap (f . (arr !)) (indices arr)
  where
    f = \case
      '#' -> Sum 1
      _ -> Sum 0

part1 :: IO ()
part1 =
  interact $
    show . countOccupied . loop False 3 . parse

part2 :: IO ()
part2 =
  interact $
    show . countOccupied . loop True 4 . parse

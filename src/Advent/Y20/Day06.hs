module Advent.Y20.Day06 where

import Data.String
import Data.List.Split (splitOn)
import qualified Data.IntSet as S
import Data.List (foldl')

part1 :: IO ()
part1 =
  interact $ show . go where
  go input = sum [count group | group <- splitOn "\n\n" input]
  count grp = S.size $ S.fromList [fromEnum ans | ppl <- lines grp, ans <- ppl]

part2 :: IO ()
part2 =
  interact $ show . go where
  go input = sum [count group | group <- splitOn "\n\n" input]
  count grp = S.size $ intersection [S.fromList $ map fromEnum ppl | ppl <- lines grp]
  intersection [] = S.empty
  intersection (x:xs) = foldl' S.intersection x xs

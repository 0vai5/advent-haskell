{-# LANGUAGE RecordWildCards, LambdaCase, DeriveGeneric #-}
module Advent.Y20.Day24 where

import qualified Commons.Parsing as P
import qualified Data.Text as T
import Data.HashSet(HashSet)
import qualified Data.HashSet as S
import Data.HashMap.Strict(HashMap)
import qualified Data.HashMap.Strict as M
import GHC.Generics (Generic)
import Data.Hashable (Hashable)
import Control.Applicative ((<|>))
import Data.List (foldl', partition)
import qualified Data.Text.IO as T

data Dir = E | W | NW | SE | NE | SW deriving (Show,Eq)
data Coord = Coord {
  x :: Double,
  y :: Double
} deriving (Eq,Show,Generic)
instance Hashable Coord

move coord@Coord{..} = \case
                E -> coord{x = x+1}
                W -> coord{x = x-1}
                SE -> coord{x = x + 0.5, y = y-1}
                NW -> coord{x = x - 0.5, y = y+1}
                NE -> coord{x = x + 0.5, y = y+1}
                SW -> coord{x = x - 0.5, y = y-1}

type BlackTiles = HashSet Coord

parser :: P.Parser [[Dir]]
parser = P.separatedBy pdirs P.newLine where
  pe = E <$ P.text "e"
  pw = W <$ P.text "w"
  pse = SE <$ P.text "se"
  pne = NE <$ P.text "ne"
  psw = SW <$ P.text "sw"
  pnw = NW <$ P.text "nw"
  pdir = pe <|> pw <|> pse <|> pne <|> psw <|> pnw
  pdirs = P.some pdir

solve1 allDirs = (S.size rs, rs) where
  moveTile = foldl' move (Coord 0 0)
  toggleColor tile blackTiles = if S.member tile blackTiles then S.delete tile blackTiles else S.insert tile blackTiles
  rs = foldl' (\blackTiles -> flip toggleColor blackTiles . moveTile ) S.empty allDirs

part1 = T.interact $ T.pack . show . solve1 . fst . head . P.parse parser

oneCycle :: BlackTiles -> BlackTiles
oneCycle blackTiles = rs where
  deltas = horz ++ dgnls where horz = [(-1,0),(1,0)]
                               dgnls = (,) <$> [-0.5,0.5] <*> [-1,1]
  nbrs tile@Coord{..} = [Coord (x+dx) (y+dy) | (dx,dy) <- deltas]
  blackRule tile = let mynbrs = nbrs tile
                       (blkNbrs,whtNbrs) = partition (`S.member` blackTiles) mynbrs
                       blkCount = length blkNbrs
                       in (blkCount == 0 || blkCount > 2 ,S.fromList whtNbrs)
  (whiteVisitedNbrsMap,newWhites)= foldl' f (M.empty,S.empty) blackTiles where
    f (whiteVisitedNbrsMap,newWhites) blkTile =
      let (toggle,whtNbrs) = blackRule blkTile
          whiteVisitedNbrsMap' = foldl' g whiteVisitedNbrsMap whtNbrs
          g accum wht = M.insertWith S.union wht (S.singleton blkTile) accum
          newWhites' = if toggle then S.insert blkTile newWhites else newWhites
      in (whiteVisitedNbrsMap',newWhites')
  newBlacks = S.fromList $ M.keys $ M.filter (\s -> S.size s == 2) whiteVisitedNbrsMap
  rs = (blackTiles `S.difference` newWhites ) `S.union` newBlacks

solve2 n = (!! n) . map S.size . iterate oneCycle . snd . solve1
part2 = T.interact $ T.pack . show . solve2 100 . fst . head . P.parse parser

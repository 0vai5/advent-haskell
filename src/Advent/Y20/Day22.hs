module Advent.Y20.Day22 where

import qualified Commons.Parsing as P
import qualified Data.Text.IO as T
import qualified Data.Text as T
import qualified Data.HashSet as S
import qualified Data.HashMap.Strict as M
import qualified Debug.Trace as Debug

data Player = P Int [Int] deriving (Show)

parser = P.separatedBy pPlayer (P.some P.newLine) where
  pId = P.text "Player " *> P.int <* P.char ':' <* P.newLine
  pCards = P.separatedBy P.int P.newLine
  pPlayer = P <$> pId <*> pCards

score = sum . zipWith (*) [1..] . reverse

solve1 (P _ pl1) (P _ pl2) = play pl1 pl2 where
  play [] pl2 = score pl2
  play pl1 [] = score pl1
  play (pl1:rmn1) (pl2:rmn2) | pl1 < pl2 = play rmn1 (rmn2 ++ [pl2,pl1])
                             | otherwise = play (rmn1 ++ [pl1,pl2]) rmn2

part1 = T.interact $ T.pack . show . solveIt . fst . head . P.parse parser where
  solveIt (p1:p2:_) = solve1 p1 p2

type Cards = ([Int],[Int])
type Winner = (Int,[Int])

solve2 (P _ pl1) (P _ pl2) = score $ snd $ play pl1 (length pl1) pl2 (length pl2) S.empty where
  play [] _ pl2 _ _ = (2,pl2)
  play pl1 _ [] _ _ = (1,pl1)
  play p1@(pl1:rmn1) n1 p2@(pl2:rmn2) n2 rounds
    | S.member (p1,p2) rounds = (1,p1)
    | pl1 < n1 && pl2 < n2 = let
                                sb_rmn1 = take pl1 rmn1
                                sb_rmn2 = take pl2 rmn2 in
                                case play sb_rmn1 (length sb_rmn1)  sb_rmn2 (length sb_rmn2) S.empty of
                                 (2,_) -> play rmn1 (n1-1) (rmn2 ++ [pl2,pl1]) (n2+1) (S.insert (p1,p2) rounds)
                                 _ -> play (rmn1 ++ [pl1,pl2]) (n1+1) rmn2 (n2-1) (S.insert (p1,p2) rounds)
    | otherwise = if pl1 < pl2 then play rmn1 (n1-1) (rmn2 ++ [pl2,pl1]) (n2+1) (S.insert (p1,p2) rounds)
                               else play (rmn1 ++ [pl1,pl2]) (n1+1) rmn2 (n2-1) (S.insert (p1,p2) rounds)

part2 = T.interact $ T.pack . show . solveIt . fst . head . P.parse parser where
  solveIt (p1:p2:_) = solve2 p1 p2

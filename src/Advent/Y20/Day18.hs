{-# LANGUAGE LambdaCase #-}
module Advent.Y20.Day18 where

import qualified Commons.Parsing as P
import qualified Data.Text as T

data Expr = I Int | Expr :+ Expr | Expr :* Expr deriving (Show)

pint = I <$> P.integer
ob = P.token $ P.char '('
cb = P.token $ P.char ')'

parser1 :: P.Parser Expr
parser1 = pexpr where
  pprn = ob *> pexpr <* cb P.<|> pint
  pop = P.token $ (:+) <$ P.char '+' P.<|> (:*) <$ P.char '*'
  pexpr = pprn >>= f
  f e = P.ifThenElse pop (\op -> pprn >>= f . op e) (pure e)

--parser for part1 and 2 can be combined but meh!
--looking at other solns it seems there is kind of a common way by parsing via levels corresponding to operator priority. But meh!
parser2 :: P.Parser Expr
parser2 = pexpr where
  pprn = ob *> pexpr <* cb P.<|> pint
  pop = P.token $ P.char '+' P.<|> P.char '*'
  pexpr = pprn >>= f
  f e = P.ifThenElse pop (\case
                             '+' -> pprn >>= f . (:+) e
                             '*' -> pexpr >>= \e' -> pure $ e :* e'
                             _ -> error "Invalid operator"
                         ) (pure e)

eval :: Expr -> Int
eval (I v) = v
eval (e1 :+ e2) = eval e1 + eval e2
eval (e1 :* e2) = eval e1 * eval e2

part1 :: IO ()
part1 = interact $ show . sum . fmap eval . parseIt where
    parseLine = fst . head . P.parse parser1
    parseIt = fmap parseLine . T.lines . T.pack

part2 :: IO ()
part2 = interact $ show . sum . fmap eval . parseIt where
    parseLine = fst . head . P.parse parser2
    parseIt = fmap parseLine . T.lines . T.pack

solve18 = P.parse

eval18a = eval . fst. head . P.parse parser1
eval18b = eval . fst. head . P.parse parser2

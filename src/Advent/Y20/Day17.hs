{-# LANGUAGE DeriveGeneric, RecordWildCards, TupleSections, BangPatterns #-}

module Advent.Y20.Day17 where

import Control.Monad.State.Strict
import Data.HashSet (HashSet)
import qualified Data.HashSet as HashSet
import Data.Hashable (Hashable)
import Data.Ix (inRange)
import GHC.Generics (Generic)
import qualified GHC.Generics
import Data.Foldable (maximumBy, minimumBy, foldl')
import Data.Ord (comparing)
import Data.List (transpose)
import Debug.Trace (trace)

newtype Point = Point [Int] deriving (Eq, Show, Generic)
data Bounds = Bounds Word [(Int,Int)] deriving (Show)
type Cubes = HashSet Point

instance Hashable Point

oneCycle :: (Cubes, Bounds) -> (Cubes, Bounds)
oneCycle (cubes, Bounds dim bnds) =
  let points =
        [ pt| t <- gen bnds,
            let pt = Point t,
            let nbrs = activeNbrs pt,
            let active = HashSet.member pt cubes,
            (active && (2, 3) `inRange` nbrs) || (not active && nbrs == 3)
        ]
      nbrs (Point pts) = map (Point . zipWith (+) pts) deltas
      activeNbrs = sum . fmap (fromEnum . flip HashSet.member cubes) . nbrs
      newBnds = fmap (\(mn,mx) -> (mn-1,mx+1)) bnds
      deltas = filter (/= rep 0) . gen $ rep (-1,1)
        where rep = replicate (fromIntegral dim)
      gen = foldr f [[]] where
        f (mn,mx) pts = [mn..mx] >>= \c -> (c:) <$> pts
   in (HashSet.fromList points, Bounds dim newBnds)

solve :: Word -> Int -> [[Char]] -> Int
solve dim n chars = rs
  where
    activePoints = fmap fst
                . filter (\(_,ch) -> ch == '#')
                . mconcat
                . zipWith (\i -> zipWith (\j -> (i:j:extraDims,)) [0 ..]) [0 ..] $ chars
    extraDims = replicate (fromIntegral dim-2) 0
    cubes = HashSet.fromList $ Point <$> activePoints
    bounds = Bounds dim $ (\xs -> (minimum xs - 1, maximum xs + 1)) <$> transpose activePoints
    cycles = iterate oneCycle (cubes, bounds)
    rs = HashSet.size . fst $ cycles !! n

testInput = [".#.", "..#", "###"]
myInput = ["#.#.##.#", "#.####.#", "...##...", "#####.##", "#....###", "##..##..", "#..####.", "#...#.#."]

part1 = solve 3 6 myInput
part2 = solve 4 6 myInput

module Advent.Y20.Day12 where

import Control.Monad.State.Strict (State, runState, get, put, StateT (runStateT))
import Control.Monad (foldM, foldM_)
import Control.Monad.Identity (Identity(runIdentity))
import Data.Foldable (foldl')

data Position = Pos Char Int Int deriving (Show)

cruise :: Char -> Int -> State Position ()
cruise ins d =
  get >>= \(Pos dir y x) -> case (ins, dir) of
    ('F', 'N') -> put $ Pos dir (y + d) x
    ('F', 'S') -> put $ Pos dir (y - d) x
    ('F', 'E') -> put $ Pos dir y (x + d)
    ('F', 'W') -> put $ Pos dir y (x - d)
    ('N', _) -> put $ Pos dir (y + d) x
    ('S', _) -> put $ Pos dir (y - d) x
    ('E', _) -> put $ Pos dir y (x + d)
    ('W', _) -> put $ Pos dir y (x - d)
    ('L', _) -> put $ Pos (computeDir aclock dir) y x
    ('R', _) -> put $ Pos (computeDir clock dir) y x
    _ -> error $ "Unexpected Char" ++ show d
  where
    computeDir dirs dir = (!! (d `div` 90)) . dropWhile (/= dir) $ dirs

aclock = 'E' : 'N' : 'W' : 'S' : aclock

clock = 'E' : 'S' : 'W' : 'N' : clock

part1 :: IO ()
part1 =
  interact $
    show
      . md
      . snd
      . flip runState (Pos 'E' 0 0)
      . foldM_ (\_ l -> cruise (head l) (read $ tail l)) ()
      . lines
      where md r@(Pos _ a b) = (r, abs a + abs b)

data Coord = Coord Int Int Int Int deriving (Show)

addCoord (Coord n e s w) (Coord n' e' s' w') = Coord (n + n') (e + e') (s + s') (w + w')

timesCoord d (Coord n e s w) = Coord (d*n) (d*e) (d*s) (d*w)

clockWise (Coord n e s w) = Coord w n e s

aclockWise (Coord n e s w) = Coord e s w n

rotate :: (Coord -> Coord) -> Int -> Coord -> Coord
rotate f times coord | times <= 0 = coord
                     | otherwise = rotate f (times-1) (f coord)

data Ship = Ship {
  wp :: Coord,
  sp :: Coord
  } deriving (Show)

wpcruise :: Char -> Int -> Ship -> Ship
wpcruise 'F' d shp@(Ship wp sp) = shp{sp=addCoord sp.timesCoord d $ wp}
wpcruise 'N' d shp@(Ship wp@(Coord n e s w) sp) = shp{wp=Coord (n+d) e s w}
wpcruise 'E' d shp@(Ship wp@(Coord n e s w) sp) = shp{wp=Coord n (e+d) s w}
wpcruise 'S' d shp@(Ship wp@(Coord n e s w) sp) = shp{wp=Coord n e (s+d) w}
wpcruise 'W' d shp@(Ship wp@(Coord n e s w) sp) = shp{wp=Coord n e s (w+d)}
wpcruise 'L' d shp@(Ship wp@(Coord n e s w) sp) = shp{wp=rotate aclockWise (d `div` 90) wp}
wpcruise 'R' d shp@(Ship wp@(Coord n e s w) sp) = shp{wp=rotate clockWise (d `div` 90) wp}
wpcruise ch _ _ = error $ "Unexpected Char" ++ show ch

part2 :: IO ()
part2 =
  interact $
    show
      . md
      . foldl' (\rs str -> wpcruise (head str) (read (tail str)) rs) (Ship (Coord 1 10 0 0) (Coord 0 0 0 0))
      . lines
      where md r@(Ship _ (Coord n e s w)) = (r, abs (n-s) + abs (e-w))

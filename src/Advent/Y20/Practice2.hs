module Advent.Y20.Practice2 where

import Control.Monad.Trans.Maybe  (MaybeT(..), runMaybeT)
import Control.Monad.Trans.Except (ExceptT(..), runExceptT)
import Control.Monad.Trans.Reader (ReaderT(..), runReaderT, Reader)
import Data.Functor.Identity
import Control.Monad
import Control.Monad.Trans.Class

-- newtype MaybeT m a = MaybeT { runMaybeT :: m (Maybe a) }
-- newtype ExceptT e m a = ExceptT { runExceptT :: m (Either e a) }
-- newtype ReaderT r m a = ReaderT { runReaderT :: r -> m a }

embedded :: MaybeT (ExceptT String (ReaderT () IO)) Int
embedded = return 1

maybeUnwrap :: ExceptT String (ReaderT () IO) (Maybe Int)
maybeUnwrap = runMaybeT embedded

eitherUnwrap :: ReaderT () IO (Either String (Maybe Int))
eitherUnwrap = runExceptT maybeUnwrap

readerUnwrap :: () -> IO (Either String (Maybe Int))
readerUnwrap = runReaderT eitherUnwrap

unwrapped :: IO (Either String (Maybe Int))
unwrapped = readerUnwrap ()

-- unwrapped = Right (Just 1)

embedded' :: MaybeT (ExceptT String (ReaderT () IO)) Int
embedded' = MaybeT $ ExceptT $ ReaderT $ return . const (Right (Just 1))

unwrapped' :: IO (Either String (Maybe Int))
unwrapped' = runReaderT (runExceptT (runMaybeT embedded')) ()

-- unwrapped' = Right (Just 1)

rDec :: Num a => Reader a a
rDec = ReaderT $ \r -> return (r-1)

rDec' :: Num a => Reader a a
rDec' = ReaderT $ return . subtract 1

rShow :: Show a => ReaderT a Identity String
rShow = ReaderT $ \r -> return $ show r

rShow' :: Show a => ReaderT a Identity String
rShow' = ReaderT $ return . show

rPrintAndInc :: (Num a, Show a) => ReaderT a IO a
rPrintAndInc = ReaderT $ \r -> putStrLn ("Hi: " ++ show r) >> return (r+1)

isValid :: String -> Bool
isValid v = '!' `elem` v

maybeExcite :: MaybeT IO String
maybeExcite =
  lift getLine >>= (\v -> guard (isValid v) >> return v)

doExcite :: IO ()
doExcite = do
  putStrLn "say something excite!"
  excite <- runMaybeT maybeExcite
  case excite of
    Nothing -> putStrLn "MOAR EXCITE"
    Just e ->
        putStrLn ("Good, was very excite: " ++ e)

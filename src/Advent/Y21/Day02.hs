{-# LANGUAGE LambdaCase #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module Advent.Y21.Day02 where

import qualified Text.ParserCombinators.ReadP as RP
import Data.Char (isDigit, isAlpha)
import Commons.Utils (alphabetsReader, intReader, newLineReader)
import Data.List

data DIRECTION = U Int | D Int | F Int deriving (Show)

parser :: RP.ReadP [DIRECTION]
parser =  RP.many1 prsr <* RP.eof where
  prsr = toDir <$> alphabetsReader <* RP.skipSpaces <*> intReader <* newLineReader
  toDir = \case
    "up" -> U
    "down" -> D
    "forward" -> F

solve1 :: [DIRECTION] -> Int
solve1 = mult . foldl' f (0,0) where
  mult (x,y) = x*y
  f (x,y) = \case
        U dy -> (x,y-dy)
        D dy -> (x,y+dy)
        F dx -> (x+dx,y)

solve2 :: [DIRECTION] -> Int
solve2 = mult . foldl' f (0,0,0) where
  mult (_,x,y) = x*y
  f (aim,x,y) = \case
    U dy -> (aim-dy,x,y)
    D dy -> (aim+dy,x,y)
    F dx -> (aim,x+dx,y+aim*dx)

io :: String -> IO ()
io fname = do
  content <- readFile fname
  let directions = fst . head . RP.readP_to_S parser $ content
  let rs1 = solve1 directions
  putStrLn $ "Day 2, Part 1: " <> show rs1

  let rs2 = solve2 directions
  putStrLn $ "Day 2, Part 2: " <> show rs2

test = io "inputs/2021/test02.txt"
main = io "inputs/2021/input02.txt"

module Advent.Y21.Day03 where

import Data.List (transpose, foldl')
import Numeric (readInt)
import qualified Data.Bifunctor
import Data.Bifunctor (Bifunctor(second))

parseBinary = fst . head . readInt 2 (const True) (fromEnum . (== '1'))

maxmin xs = let (zeroes,ones) = foldl' g (0,0) xs
                g (zs,ns) ch = if ch == '0' then (zs+1,ns) else (zs,ns+1)
                in if zeroes > ones then ('0','1') else ('1','0')

solve1 :: [String] -> Int
solve1 inp = parseBinary gamma * parseBinary epsilon where
  (gamma,epsilon) = foldr f ([],[]) (transpose inp)
  f xs (gamma,epsilon) = let (mx,mn) = maxmin xs
                          in (mx:gamma,mn:epsilon)

solve2 inp = parseBinary o2 * parseBinary co2 where
  cycle f = fst . head . until (null . tail) f
  freqFilter freq xss = second tail <$> filter ((== fr) . head . snd) xss
    where fr = freq $ head . snd <$> xss
  indexedInp = zip [0..] inp
  o2 = inp !! cycle o2Filter indexedInp where o2Filter = freqFilter (fst . maxmin)
  co2 = inp !! cycle co2Filter indexedInp where co2Filter = freqFilter (snd . maxmin)

io :: String -> IO ()
io fname = do
  binStrings <- lines <$> readFile fname

  let rs1 = solve1 binStrings
  putStrLn $ "Day 3, Part 1: " <> show rs1

  let rs2 = solve2 binStrings
  putStrLn $ "Day 3, Part 2: " <> show rs2

test = io "inputs/2021/test03.txt"
main = io "inputs/2021/input03.txt"

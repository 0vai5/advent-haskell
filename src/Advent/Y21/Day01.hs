module Advent.Y21.Day01 where

import qualified Data.Text as T
import qualified Data.Text.IO as T
import Commons.Utils (parseIntegral)

solve1 xs = length $ filter id $ zipWith (<) xs (tail xs)

part1 =
  T.interact $
    T.pack . show . fmap solve1 . traverse parseIntegral . T.lines

solve2 xs = solve1 $ zipWith3 (\a b c -> a + b + c) xs (tail xs) (tail (tail xs))

part2 :: IO ()
part2 =
  T.interact $
    T.pack . show . fmap solve2 . traverse parseIntegral . T.lines

-- from reddit. first part is obvious but for second the idea is: if (a+b+c) < (b+c+d) then a < d
solve :: Int -> [Int] -> Int
solve n inp = sum [1 | (a, b) <- zip inp (drop n inp), a < b]
day1p1 = solve 1
day1p2 = solve 3

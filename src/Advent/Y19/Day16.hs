{-# LANGUAGE BangPatterns #-}

module Advent.Y19.Day16 where

import Data.Char
import Debug.Trace

compDigit :: [Int] -> [Int] -> Int -> Int
compDigit pttrn !digits n = abs rs `rem` 10 where
  repeat x = take n $ cycle [x]
  repPttrn = tail $ cycle $ pttrn >>= repeat
  !rs = foldr (\(d,p) r -> r + d * p) 0 $ zip digits repPttrn

solve :: [Int] -> [Int] -> [Int]
solve pttrn !digits = map (\n -> compDigit pttrn digits n) [1..length digits]

series :: [Int] -> [Int] -> [[Int]]
series pttrn digits = digits:series pttrn (solve pttrn digits)

pttrn :: [Int]
pttrn = [0,1,0,-1]

test1 :: [Int]
test1 = [1,2,3,4,5,6,7,8]

inputStr :: String
inputStr = "59701570675760924481468012067902478812377492613954566256863227624090726538076719031827110112218902371664052147572491882858008242937287936208269895771400070570886260309124375604950837194732536769939795802273457678297869571088110112173204448277260003249726055332223066278888042125728226850149451127485319630564652511440121971260468295567189053611247498748385879836383604705613231419155730182489686270150648290445732753180836698378460890488307204523285294726263982377557287840630275524509386476100231552157293345748976554661695889479304833182708265881051804444659263862174484931386853109358406272868928125418931982642538301207634051202072657901464169114"

input :: [Int]
input = map digitToInt inputStr

test2Str = "03036732577212944063491565474664"

test2 :: [Int]
test2 = map digitToInt test2Str

test2' :: [Int]
test2' = drop 303673 $ take (10000 * length test2) $ cycle test2

-- different from standard scanr:
-- this return empty list if empty list is passed
-- or it does not return b in the end of list
myscanr :: (a -> b -> b) -> b -> [a] -> [b]
myscanr _ _ [] = []
myscanr f b (x:[]) = [f x b]
myscanr f b (x:xs) = f x (head rs):rs where
  rs = myscanr f b xs

largeInputStr :: String
largeInputStr = drop 5970157 $ take (10000 * (length inputStr)) $ cycle inputStr

largeInput :: [Int]
largeInput = map digitToInt largeInputStr

-- this gave the answer for part2
-- take 8 $ (iterate (myscanr (\a b -> (a+b) `rem` 10) 0) largeInput !! 100)

module Advent.Y19.Day02 where

import Data.List.Split

replaceVal :: Int -> a -> [a] -> [a]
replaceVal _ _ [] = []
replaceVal 0 val (x:xs) = val:xs
replaceVal pos val (x:xs) = x: replaceVal (pos-1) val xs

alarm :: Int -> [Int] -> [Int] -> [Int]
alarm _ (99:_) rs = rs
alarm c (op:pos1:pos2:pos3:_) rs = alarm (c+1) opList newList where
  opList = drop ((c+1)*4) newList
  newList = replaceVal pos3 newVal rs
  newVal = val1 `func` val2
  val1 = rs !! pos1
  val2 = rs !! pos2
  func = case op of
             1 -> (+)
             2 -> (*)
             _ -> error "Error"
alarm _ _ _ = error "Error!"

solve01 :: String -> IO ()
solve01 inputFile = do
  input <- readFile inputFile
  let vals = map read $ splitOn "," input
  let newVals = (replaceVal 2 2 . replaceVal 1 12) vals
  let rs = alarm 0 newVals newVals
  putStrLn $ show rs

cart :: [a] -> [a] -> [(a,a)]
cart xs ys = do
  x <- xs
  y <- ys
  return (x,y)

part02 :: Int -> [Int] -> Int
part02 val input = 100 * noun + verb where
  replace (n,v) = (replaceVal 1 n . replaceVal 2 v) input
  (noun,verb) = head $
                filter (\pair ->
                           let newInput = replace pair in
                             (head $ alarm 0 newInput newInput) == val)
                $ cart [0 .. 99] [0 .. 99]

solve02 :: String -> IO ()
solve02 inputFile = do
  input <- readFile inputFile
  let vals = map read $ splitOn "," input
  let rs = part02 19690720 vals
  putStrLn $ show rs

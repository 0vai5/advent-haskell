{-# LANGUAGE BangPatterns #-}

module Advent.Y19.Day14 where

import qualified Data.Map as M
import Data.List.Split
import Debug.Trace

type Element = String

type Quantum = (Element,Integer)

data Reaction = R [Quantum] Quantum deriving Show

type RTable = M.Map Element Reaction

type Quantities = M.Map Element Integer

mkQuantum :: RTable -> Quantum -> Quantities
mkQuantum rtable (!elem, !reqCount)
  | reqCount <= 0 = M.fromList [(elem, reqCount)]
  | otherwise = rs where
      (Just (R qntms (_, createdCount))) = M.lookup elem rtable
      !times = if reqCount `rem` createdCount == 0
              then q
              else q + 1 where q = reqCount `div` createdCount
      !extra = times * createdCount - reqCount
      !rs = M.fromListWith (+) $ (elem,(-extra)):(map (\(el, n) -> (el, n * times)) qntms)

mkQuantums :: RTable -> Quantities -> Quantities
mkQuantums rtable qnts =
  if M.size rqrd > 0
  then
    mkMore rqrd extra
  else
    extra
  where
  (rqrd, extra) = M.partitionWithKey (\k v -> k /= "ORE" && v > 0) qnts
  mkMore :: Quantities -> Quantities -> Quantities
  mkMore !rqrd !extra = mkQuantums rtable rs where
    !rs = M.unionWith (+) extra newQnts
    newQnts = M.unionsWith (+) $ map (mkQuantum rtable) (M.toList rqrd)
  
fuel :: RTable -> Integer -> Integer
fuel rtable fuelAmt = n where
  (Just n) = M.lookup "ORE" $ mkQuantums rtable (M.singleton "FUEL" fuelAmt)

parseLine :: String -> (Element, Reaction)
parseLine line = (elm, R qntms qntm) where
  rctn = splitOn "=>" line
  fromElms = head rctn
  toElm = head $ tail rctn
  mkQntm str = case words str of
    n:el:[] -> (el, read n)
  qntms = map mkQntm $ splitOn "," fromElms
  qntm@(elm,_) = mkQntm toElm 

part01 :: String -> IO ()
part01 fname = do
  input <- readFile fname
  let rtable = M.fromList $ map parseLine $ lines input
      rs = fuel rtable 1 in
      -- rs = mkQuantums rtable (M.fromList [("FUEL", 1), ("A",(-26)), ("B", (-1))]) in
      -- rs = mkQuantum rtable ("A",7) in
    putStrLn $ show rs

maxFuel :: RTable -> Integer -> Integer
maxFuel rtable oresCount = traceShow(lowFuelLimit, upperFuelLimit, rs, fuel rtable rs) rs where
  (i,lowFuelLimit) = head $ reverse $ takeWhile (\(_,f) -> fuel rtable f <= oresCount) [(i,2^i) | i <- [0..]]
  upperFuelLimit = 2^(i+1)
  search !lowFuelLimit !upperFuelLimit | lowFuelLimit == upperFuelLimit = lowFuelLimit
                | otherwise = if fuel rtable mid > oresCount
                              then search lowFuelLimit mid
                              else search (mid+1) upperFuelLimit where
                    mid = (lowFuelLimit + upperFuelLimit) `div` 2
  -- -1 bec in search we find the first fuel ammount that requires more than oresCount. Thus -1 give the correct answer.
  -- we can also do reverse, that is first fuel amount that requires less than oresCount. Then we need to add +1
  -- we can't search for == ofcourse :)
  !rs = search lowFuelLimit upperFuelLimit - 1
    
part02 :: String -> IO ()
part02 fname = do
  input <- readFile fname
  let rtable = M.fromList $ map parseLine $ lines input
      rs = maxFuel rtable 1000000000000 in
      -- rs = mkQuantums rtable (M.fromList [("FUEL", 1), ("A",(-26)), ("B", (-1))]) in
      -- rs = mkQuantum rtable ("A",7) in
    putStrLn $ show rs
  
fname = "/home/t0xy/haskell/advent-haskell/inputs/2019/input14.txt"
test1 = "/home/t0xy/haskell/advent-haskell/inputs/2019/test14-p1-1.txt"
test2 = "/home/t0xy/haskell/advent-haskell/inputs/2019/test14-p1-2.txt"
test3 = "/home/t0xy/haskell/advent-haskell/inputs/2019/test14-p1-3.txt"
test4 = "/home/t0xy/haskell/advent-haskell/inputs/2019/test14-p1-4.txt"
test5 = "/home/t0xy/haskell/advent-haskell/inputs/2019/test14-p1-5.txt"

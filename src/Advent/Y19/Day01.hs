module Advent.Y19.Day01 where

fuelreq01 :: Int -> Int
fuelreq01 m = m `div` 3 - 2

fuelreq02 :: Int -> Int
fuelreq02 m = if f <= 0 then 0
              else f + fuelreq02 f
  where f = fuelreq01 m

solve :: (Int -> Int) -> String -> IO ()
solve calc inputFile = do
  input <- readFile inputFile
  let rs = foldr ((+) . calc . read) 0 $ (lines input)
  putStrLn $ show rs

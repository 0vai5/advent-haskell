{-# LANGUAGE BangPatterns #-}
module Advent.Y19.Day18 where

import qualified Data.Map as M
import qualified Data.Set as S
import Data.Char
import Control.Monad (join)
import Debug.Trace

data Tile = Start | Open | Wall | Door Char | Key Char deriving (Eq, Show, Ord)
type Point = (Int,Int)
type Maze = M.Map Point Tile
type PointTile = (Point,Tile)
data Distance = D PointTile Int deriving (Eq,Show)
type Keys = S.Set Tile
type Table = M.Map (Keys,PointTile) Int

neighbours :: Maze -> PointTile -> [PointTile]
neighbours maze (!(x,y),_) = 
  join $ map (\point -> case M.lookup point maze of
                 Just Wall -> []
                 Just tile -> [(point, tile)]
                 Nothing -> []
             )
             [(x-1,y),(x+1,y),(x,y-1),(x,y+1)]

data KeyDoorStatus = VK| VD| NK| ND| NKD deriving (Eq,Show)

visitedKeyOrDoor :: Keys -> PointTile -> KeyDoorStatus
visitedKeyOrDoor keys (_, key@(Key _)) | S.member key keys = VK
                                       | otherwise = NK
visitedKeyOrDoor keys (_, Door d) | S.member (Key $ toLower d) keys = VD
                                  | otherwise = ND
visitedKeyOrDoor _ _ = NKD

reachableKeys :: Maze -> Keys -> PointTile -> [Distance]
reachableKeys maze keysFound from = bfs 0 [from] (S.singleton from) where
  bfs :: Int -> [PointTile] -> S.Set PointTile -> [Distance]
  bfs currDist [] considered = []
  bfs !currDist !toCheck !considered = rs where
    rs = keysndoors ++ bfs (currDist+1) toCheck' considered'
    --remove duplicates
    toCheck' = S.toList $ S.fromList newForCheck
    considered' = foldr (S.insert) considered toCheck'
    (keysndoors, newForCheck) = foldr
                                (\pointtile (keysndoors,newForCheck) ->
                                    let !status = visitedKeyOrDoor keysFound pointtile
                                        keysndoors' = if status == NK
                                                      then (D pointtile currDist):keysndoors
                                                      else keysndoors
                                        newForCheck' = case status of
                                                         NK -> newForCheck
                                                         ND -> newForCheck
                                                         _ -> newForCheck ++
                                                              filter
                                                                 (\nbr -> not $ S.member nbr considered)
                                                                 (neighbours maze pointtile)
                                          in (keysndoors', newForCheck'))
                                ([],[])
                                toCheck
    
shortest :: Maze -> Keys -> PointTile -> Table -> (Table,Int)
shortest maze !keysFound !pointTile@(_,fromTile) table =
  case M.lookup (keysFound,pointTile) table of
    Just sd -> (table,sd)
    Nothing -> ((M.insert (keysFound,pointTile) sd table'), sd) where
      allReachables = reachableKeys maze keysFound' pointTile
      sdMax = if allReachables == [] then 0 else maxBound
      (table',sd) = foldr (\(D rpt dist) (tbl,sd) ->
                              case shortest maze keysFound' rpt tbl of
                                (tb, d) -> (tb, min sd (d+dist)))
                     (table, sdMax)
                     allReachables
  where
    keysFound' = case pointTile of
      (_,Key _) -> case S.member fromTile keysFound of
                     True -> error ("Not Possible" ++ (show fromTile) ++ (show keysFound))
                     False -> S.insert fromTile keysFound
      (_,Start) -> keysFound
      _ -> error $ "Should be starting point or key : " ++ show pointTile

readMaze :: String -> Maze
readMaze input = M.fromList $ join rs where
  makeTile '.' = Open
  makeTile '#' = Wall
  makeTile '@' = Start
  makeTile  ch  | isLower ch = Key ch
                | otherwise = Door ch
  zipIndex :: [a] -> [(Int,a)]
  zipIndex = zip [0..]
  rs = map (\(lno,l) -> map (\(cno,c) -> ((cno,lno),makeTile c)) (zipIndex l)) (zipIndex $ lines input)

keysAndStartingPoint :: Maze -> (Keys, PointTile)
keysAndStartingPoint maze = foldr (\pt (keys,sp) ->
                                     case pt of
                                       (_, key@(Key _)) -> (S.insert key keys,sp)
                                       (_,Start) -> (keys,pt)
                                       _ -> (keys,sp))
                            (S.empty,((-2,-2),Start))
                            (M.toList maze)

part01 :: String -> IO()
part01 fname = do
  input <- readFile fname
  let maze = readMaze input
  let (allKeys,st) = keysAndStartingPoint maze
  putStrLn "STARTING POINT: "
  putStrLn $ show st
  let (tbl,sd) = shortest maze S.empty st (M.singleton (allKeys,st) 0)
  putStrLn $ "SHORTEST : "
  putStrLn $ show sd
  putStrLn $ "TABLE SIZE : "
  putStrLn $ show $ M.size $ tbl
  putStrLn $ "TABLE : "
  putStrLn $ show $ if M.size tbl < 200 then tbl else M.empty


test1 = "/home/t0xy/haskell/advent-haskell/inputs/2019/test18-p1-1.txt"
test2 = "/home/t0xy/haskell/advent-haskell/inputs/2019/test18-p1-2.txt"
test3 = "/home/t0xy/haskell/advent-haskell/inputs/2019/test18-p1-3.txt"
test4 = "/home/t0xy/haskell/advent-haskell/inputs/2019/test18-p1-4.txt"
reddit = "/home/t0xy/haskell/advent-haskell/inputs/2019/test18-both-reddit.txt"

input = "/home/t0xy/haskell/advent-haskell/inputs/2019/input18.txt"

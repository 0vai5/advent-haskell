module Advent.Y19.Day06 where

import qualified Data.Map as M
import Data.List.Split

createTuples :: [String] -> [(String, String)]
createTuples xs = map (\s -> let r = splitOn ")" s
                                 a = head r
                                 b = head $ tail r
                             in (b,a)) xs

orbiting:: [(String, String)] -> M.Map String String
orbiting = M.fromList

total :: M.Map String String -> String -> Int
total mmap v = case M.lookup v mmap of
                 Nothing -> 0
                 Just a -> 1 + total mmap a

solve01 :: String -> IO ()
solve01 inputFile = do
  input <- readFile inputFile
  let vals = lines input
  let alltuples = createTuples vals
  let omap = orbiting alltuples
  let rs = sum $ map (total omap) (M.keys omap)
  putStrLn $ show rs

orbitBy :: [(String, String)] -> M.Map String [String]
orbitBy tuples = M.fromListWith (++) (map (\(a,b) -> (b,[a])) tuples)

solve02 :: String -> String -> M.Map String String -> M.Map String [String] -> Maybe Int
solve02 from to omap obmap = findByOrbit from st 0 where
  st = case M.lookup from omap of
         Just v -> v
         Nothing -> undefined
  findByOrbit prev curr n = if curr == to
                            then Just n
                            else case M.lookup curr omap of
                                   Just a -> case findByOrbit curr a (n+1) of
                                               Just n -> Just n
                                               Nothing -> findByOrbitBy curr a (n+1)
                                   Nothing -> findByOrbitBy prev curr n
  findByOrbitBy prev curr n =
    if curr == to
    then Just (n-1)
    else case M.lookup curr obmap of
           Just xs -> foldr (\e rs ->
                                case (e,rs) of
                                  (Nothing, Just nn) -> Just nn
                                  (Just nn, Nothing) -> Just nn
                                  (Nothing, Nothing) -> Nothing)
                      Nothing
                      $ map (\x -> findByOrbitBy prev x (n+1)) (filter (\x -> x /= prev) xs)
           Nothing -> Nothing

part02 :: String -> String -> String -> IO ()
part02 from to inputFile = do
  input <- readFile inputFile
  let vals = lines input
  let alltuples = createTuples vals
  let omap = orbiting alltuples
  let obmap = orbitBy alltuples
  let rs = solve02 from to omap obmap
  putStrLn $ show rs

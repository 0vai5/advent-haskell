{-# LANGUAGE BangPatterns #-}
module Advent.Y19.Day20p2 where

import qualified Data.Map as M
import qualified Data.Set as S
import Control.Monad (join)
import Debug.Trace

type Name = String
data Tile = Open | Wall deriving (Eq, Show, Ord)
type Point = (Int,Int)
type Maze = M.Map Point Tile
type PointTile = (Point,Tile)
data Distance = D PointTile Int deriving (Eq,Show)
type Keys = S.Set Tile

data Type = IN | OUT deriving (Eq,Show,Ord)
type PortalConnections = M.Map Point (Point,Type)
type PortalTable = M.Map Name [(Point,Type)]
type Level = Int
type LPoint = (Point, Level)

type RawMaze = [(Point, Char)]

--IN to Out Level add
--Out to In level minus but can't be < 0

part02 :: String -> IO()
part02 fname = do
  content <- readFile fname
  putStrLn "Reading raw data: "
  let rawMaze = readRaw content
  -- putStrLn $ show rawMaze
  putStrLn "Building Portal Table.. "
  let portalTable = readPortals rawMaze
  putStrLn "Finding Start and End: "
  let ((st,_), (end,_)) = case (M.lookup "AA" portalTable, M.lookup "ZZ" portalTable) of
        (Just l1, Just l2) -> (head l1, head l2)
        _ -> error "Start, End not found!"
  putStrLn $ show $ ("Start: ", st, "   End: ", end)
  putStrLn "Building Portal Connections.."
  let ptlConn = mkPortalConnections portalTable
  putStrLn $ "Number of Portal Connections " ++ show (M.size ptlConn)
  putStrLn "Building Maze.."
  let maze = readMaze rawMaze
  -- putStrLn $ show maze
  putStrLn "Finding shortest..."
  let rs = shortest maze ptlConn (st,0) (end,0)
  putStrLn $ show rs

shortest :: Maze -> PortalConnections -> LPoint -> LPoint -> Maybe Int
shortest maze pc st end = bfs 0 [st] (S.singleton st) where
  --assumes point passed is Open
  neighbours :: LPoint -> [LPoint]
  neighbours lpt@(pt@(x,y),lvl) = case M.lookup pt maze of
    Just Open -> nbrsAndportals where
      nbrs = filter (\(p,lvl) -> case M.lookup p maze of
                             Just Open -> True
                             _ -> False)
             [((x-1,y),lvl),((x+1,y),lvl),((x,y-1),lvl),((x,y+1),lvl)]
      nbrsAndportals = case M.lookup pt pc of
                         Just (toPt,OUT) -> (toPt,lvl+1):nbrs
                         Just (toPt,IN) -> if lvl == 0 then nbrs else (toPt,lvl-1):nbrs
                         _ -> nbrs
    _ -> [] 
  bfs :: Int -> [LPoint] -> S.Set LPoint -> Maybe Int
  bfs _ [] _ = Nothing
  bfs dist toCheck considered = --traceShow(dist, " TC: ", toCheck, " CD: ", considered)
    if end `elem` toCheck
    then Just dist
    else bfs (dist+1) newToCheck newConsidered where
      newToCheck = filter (flip S.notMember considered) $
                   join $ map (neighbours) toCheck
      newConsidered = foldr (S.insert) considered newToCheck


readMaze :: RawMaze -> Maze
readMaze = M.fromList . (=<<) (\(pt,ch) ->
                                  case ch of
                                    '.' -> [(pt,Open)]
                                    '#' -> [(pt,Wall)]
                                    _ -> [])

--it assumes that point passed to it is portal point
--i.e. '.' point near to the portal
mkPortalTypeFinder :: RawMaze -> Point -> Type
mkPortalTypeFinder rawMaze = typeTester where
  reverseMaze = reverse rawMaze
  ((outerStCol, outerStRow),_) = head $
                                 dropWhile (\(_,ch) -> ch /= '#') rawMaze
  ((outerEndCol, outerEndRow),_) = head $
                                   dropWhile (\(_,ch) -> ch /= '#') reverseMaze
  typeTester :: Point -> Type
  typeTester (c,r) =
    if (r == outerStRow || r == outerEndRow) && (c >= outerStCol && c <= outerEndCol)
    then OUT
    else if (c == outerStCol || c == outerEndCol) && (r >= outerStRow && r <= outerEndRow)
    then OUT
    else IN
    
  
                   
readPortals :: RawMaze -> PortalTable
readPortals rawMaze = M.fromListWith (++) portalsNamePos where
  typeFinder = mkPortalTypeFinder rawMaze
  readPortalName' = readPortalName rawMaze
  portalsNamePos :: [(String,[(Point,Type)])]
  portalsNamePos = do
    (point,ch) <- rawMaze
    case readPortalName' (point,ch) of
      Just name -> [(name,[(point, typeFinder point)])]
      Nothing -> []

mkPortalConnections :: PortalTable -> PortalConnections
mkPortalConnections table = M.fromList $ do
  (_, portalPoints) <- M.toList table
  case portalPoints of
    (ptl1@(pt1,t1):ptl2@(pt2,t2):[]) -> [(pt1,ptl2),(pt2,ptl1)]
    _ -> []

-- if the given point is '.' then it returns the portal if there is any
readPortalName :: RawMaze -> (Point, Char) -> Maybe Name
readPortalName rawMaze (point, '.') = maybename where
  isPortalChar :: Char -> Bool
  isPortalChar !ch = ch /= '.' && ch /= '#' && ch /= ' '
  rawMazeMap :: M.Map Point Char
  rawMazeMap = M.fromList rawMaze
  namesList :: [Name]
  !namesList = foldMap (\(p1,p2) ->
                            case (M.lookup p1 rawMazeMap, M.lookup p2 rawMazeMap) of
                              (Just c1, Just c2) -> if isPortalChar c1 && isPortalChar c2
                                                    then [[c1,c2]]
                                                    else []
                              _ -> [])
               (possiblePortalPositions point)
  !maybename = case namesList of
                [] -> Nothing
                name:[] -> Just name
                _ -> error $ "Multiple names found" ++ (show namesList)
readPortalName _ (_, _) = Nothing

possiblePortalPositions :: Point -> [(Point,Point)]
possiblePortalPositions (x,y) = [((x-2,y),(x-1,y)),
                                 ((x+1,y),(x+2,y)),
                                 ((x,y-2),(x,y-1)),
                                 ((x,y+1),(x,y+2))]

readRaw :: String -> RawMaze
readRaw input = join rs where
  zipIndex :: [a] -> [(Int,a)]
  zipIndex = zip [0..]
  rs = map (\(lno,l) -> map (\(cno,c) -> ((cno,lno),c)) (zipIndex l)) (zipIndex $ lines input)

test1 = "/home/t0xy/haskell/advent-haskell/inputs/2019/test20-p1-1.txt"
test2 = "/home/t0xy/haskell/advent-haskell/inputs/2019/test20-p1-2.txt"
test3 = "/home/t0xy/haskell/advent-haskell/inputs/2019/test20-p2-1.txt"
input = "/home/t0xy/haskell/advent-haskell/inputs/2019/input20.txt"

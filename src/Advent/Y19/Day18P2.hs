{-# LANGUAGE BangPatterns #-}
module Advent.Y19.Day18P2 where

import qualified Data.Map as M
import qualified Data.Set as S
import Data.Char
import Control.Monad (join)
import Debug.Trace

data Tile = Start | Open | Wall | Door Char | Key Char deriving (Eq, Show, Ord)
type Point = (Int,Int)
type Maze = M.Map Point Tile
type PointTile = (Point,Tile)
data Distance = D PointTile Int deriving (Eq,Show)
type Keys = S.Set Tile
type Table = M.Map (Keys,RobotVector) Int
data RobotVector = R PointTile PointTile PointTile PointTile deriving (Eq,Show,Ord)

neighbours :: Maze -> PointTile -> [PointTile]
neighbours maze (!(x,y),_) =
  join $ map (\point -> case M.lookup point maze of
                 Just Wall -> []
                 Just tile -> [(point, tile)]
                 Nothing -> []
             )
             [(x-1,y),(x+1,y),(x,y-1),(x,y+1)]

data KeyDoorStatus = VK| VD| NK| ND| NKD deriving (Eq,Show)

visitedKeyOrDoor :: Keys -> PointTile -> KeyDoorStatus
visitedKeyOrDoor keys (_, key@(Key _)) | S.member key keys = VK
                                       | otherwise = NK
visitedKeyOrDoor keys (_, Door d) | S.member (Key $ toLower d) keys = VD
                                  | otherwise = ND
visitedKeyOrDoor _ _ = NKD

reachableKeys :: Maze -> Keys -> PointTile -> [Distance]
reachableKeys maze keysFound from = bfs 0 [from] (S.singleton from) where
  bfs :: Int -> [PointTile] -> S.Set PointTile -> [Distance]
  bfs currDist [] considered = []
  bfs !currDist !toCheck !considered = rs where
    rs = keysndoors ++ bfs (currDist+1) toCheck' considered'
    --remove duplicates
    toCheck' = S.toList $ S.fromList newForCheck
    considered' = foldr (S.insert) considered toCheck'
    (keysndoors, newForCheck) = foldr
                                (\pointtile (keysndoors,newForCheck) ->
                                    let !status = visitedKeyOrDoor keysFound pointtile
                                        keysndoors' = if status == NK
                                                      then (D pointtile currDist):keysndoors
                                                      else keysndoors
                                        newForCheck' = case status of
                                                         NK -> newForCheck
                                                         ND -> newForCheck
                                                         _ -> newForCheck ++
                                                              filter
                                                                 (\nbr -> not $ S.member nbr considered)
                                                                 (neighbours maze pointtile)
                                          in (keysndoors', newForCheck'))
                                ([],[])
                                toCheck

shortest :: Maze -> Keys -> RobotVector -> Table -> (Table,Int)
shortest maze !keysFound rv@(R rb1 rb2 rb3 rb4) table =
  case M.lookup (keysFound,rv) table of
    Just sd -> (table,sd)
    Nothing -> ((M.insert (keysFound,rv) sd table'), sd) where
      allReachables = do
        (D ptRb1 d1) <- case reachableKeys maze keysFound' rb1 of
                          [] -> [D rb1 0]
                          rs -> rs
        (D ptRb2 d2) <- case reachableKeys maze keysFound' rb2 of
                          [] -> [D rb2 0]
                          rs -> rs
        (D ptRb3 d3) <- case reachableKeys maze keysFound' rb3 of
                          [] -> [D rb3 0]
                          rs -> rs
        (D ptRb4 d4) <- case reachableKeys maze keysFound' rb4 of
                          [] -> [D rb4 0]
                          rs -> rs
        return (d1+d2+d3+d4, R ptRb1 ptRb2 ptRb3 ptRb4)
      filteredReachables = filter (\(totD,_) -> totD /= 0) allReachables
      sdMax = if null filteredReachables then 0 else maxBound
      (table',sd) = foldr (\(totD, rvect) (tbl,sd) ->
                              case shortest maze keysFound' rvect tbl of
                                (tb, d) -> (tb, min sd (d+totD)))
                     (table, sdMax)
                     filteredReachables
  where
    keysFound' = foldr (\pointTile@(_,fromTile) keys ->
                        case pointTile of
                          (_,Key _) -> case S.member fromTile keys of
                                         True -> -- error ("Not Possible" ++ (show fromTile) ++ (show keys))
                                           keys
                                         False -> S.insert fromTile keys
                          (_,Start) -> keys
                          _ -> error $ "Should be starting point or key : " ++ show pointTile)
                 keysFound
                 [rb1,rb2,rb3,rb4]

readMaze :: String -> Maze
readMaze input = M.fromList $ join rs where
  makeTile '.' = Open
  makeTile '#' = Wall
  makeTile '@' = Start
  makeTile  ch  | isLower ch = Key ch
                | otherwise = Door ch
  zipIndex :: [a] -> [(Int,a)]
  zipIndex = zip [0..]
  rs = map (\(lno,l) -> map (\(cno,c) -> ((cno,lno),makeTile c)) (zipIndex l)) (zipIndex $ lines input)

keysAndStartingPoint :: Maze -> (Keys, PointTile)
keysAndStartingPoint maze = foldr (\pt (keys,sp) ->
                                     case pt of
                                       (_, key@(Key _)) -> (S.insert key keys,sp)
                                       (_,Start) -> (keys,pt)
                                       _ -> (keys,sp))
                            (S.empty,((-2,-2),Start))
                            (M.toList maze)

-- adds robots to maize
modifyMaize :: Maze -> PointTile -> (Maze,RobotVector)
modifyMaize maize ((x,y),Start) = (maizeWithWalls,robotVector) where
  newStarts = [(x-1,y-1),(x+1,y-1),(x-1,y+1),(x+1,y+1)]
  newWalls = [(x,y),(x-1,y),(x+1,y),(x,y-1),(x,y+1)]
  maizeWithStarts = foldr
                    (\point -> M.insert point Start)
                    maize
                    newStarts
  maizeWithWalls = foldr
                    (\point -> M.insert point Wall)
                    maizeWithStarts
                    newWalls
  robotVector = case newStarts of
    (p1:p2:p3:p4:[]) -> R (p1,Start) (p2,Start) (p3,Start) (p4,Start)

part02 :: String -> IO()
part02 fname = do
  input <- readFile fname
  let maize = readMaze input
  let (allKeys,st) = keysAndStartingPoint maize
  putStrLn "STARTING POINT: "
  putStrLn $ show st
  let (modMaize,rv) = modifyMaize maize st
  putStrLn "Modified Maize: "
  putStrLn $ show modMaize
  let (tbl,sd) = shortest modMaize S.empty rv (M.singleton (allKeys,rv) 0)
  putStrLn $ "SHORTEST : "
  putStrLn $ show sd
  putStrLn $ "TABLE SIZE : "
  putStrLn $ show $ M.size $ tbl
  putStrLn $ "TABLE : "
  putStrLn $ show $ if M.size tbl < 200 then tbl else M.empty


test1 = "/home/t0xy/haskell/advent-haskell/inputs/2019/test18-p2-1.txt"
test2 = "/home/t0xy/haskell/advent-haskell/inputs/2019/test18-p2-2.txt"
test3 = "/home/t0xy/haskell/advent-haskell/inputs/2019/test18-p2-3.txt"
reddit = "/home/t0xy/haskell/advent-haskell/inputs/2019/test18-both-reddit.txt"

input = "/home/t0xy/haskell/advent-haskell/inputs/2019/input18.txt"

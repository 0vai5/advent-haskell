module Advent.Y19.Day12 where

import Debug.Trace

data Position = P Int Int Int deriving Show
data Velocity = V Int Int Int deriving Show

data Moon = M Position Velocity deriving Show

velocity :: Moon -> [Moon] -> Velocity
velocity (M (P x y z) vel) =
  foldr
   (\(M (P x' y' z') _) (V vx vy vz) -> V (inc x x' vx) (inc y y' vy) (inc z z' vz))
   vel where
  inc a a' v = if a < a' then v + 1
               else if a > a' then v - 1
                    else v

position :: Position -> Velocity -> Position
position (P x y z) (V vx vy vz) = P (x+vx) (y+vy) (z + vz)

step :: Moon -> [Moon] -> Moon
step moon@(M mp _) moons = M (position mp newVel) newVel where
  newVel = velocity moon moons

aRound :: [Moon] -> [Moon]
aRound moons = let count = length moons
                   repeated = cycle moons in
                do
                  i <- [1..count]
                  let moon = moons !! (i-1)
                  let others = take (count -1) $ drop i repeated
                  return $ step moon others

nRounds :: [Moon] -> Int -> [Moon]
nRounds = (!!) . iterate aRound

totalEnergy :: Moon -> Int
totalEnergy (M (P x y z) (V vx vy vz)) = pe * ke where
  pe = abs x + abs y + abs z
  ke = abs vx + abs vy + abs vz

-- For part1 used the result of following
-- sum $ map totalEnergy $ nRounds input 1000

test1 = [
  M (P (-1) 0 2)      (V 0 0 0),
  M (P 2 (-10) (-7))  (V 0 0 0),
  M (P 4 (-8) 8)      (V 0 0 0),
  M (P 3 5 (-1))      (V 0 0 0)]
  
test2 = [
  M (P (-8) (-10) 0)      (V 0 0 0),
  M (P 5 5 10)            (V 0 0 0),
  M (P 2 (-7) 3)          (V 0 0 0),
  M (P 9 (-8) (-3))       (V 0 0 0)]

input = [
  M (P 16 (-11) 2)      (V 0 0 0),
  M (P 0 (-4) 7)        (V 0 0 0),
  M (P 6 4 (-10))       (V 0 0 0),
  M (P (-3) (-2) (-4))  (V 0 0 0)]

-- part-2

data Dim = X | Y | Z

dim :: Dim -> Moon -> Int
dim X (M (P x _ _) _) = x
dim Y (M (P _ y _) _) = y
dim Z (M (P _ _ z) _) = z

-- Adding two bec (1) we are moving one dim while calling iterate
-- (2) and we are not counting the point where we get the repeation in  the nonRepeating list
countInDim :: Dim -> [Moon] -> Int
countInDim d moons = 2 + length notRepeating where
  extract = map (dim d)
  starting = extract moons
  notRepeating = takeWhile (\mns -> starting /= extract mns) (iterate aRound (aRound moons))

lcmOfThree :: Int -> Int -> Int -> Int
lcmOfThree a b c = traceShow(a,b,c) lcm a $ lcm b c

findLcm :: Int -> Int -> Int
findLcm a b = head $ filter (\ma -> ma `rem` b == 0) [i*a | i <- [1..]]

solve :: [Moon] -> Int
solve moons = lcmOfThree cx cy cz where
  cx = countInDim X moons
  cy = countInDim Y moons
  cz = countInDim Z moons
  

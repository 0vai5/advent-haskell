{-# LANGUAGE BangPatterns #-}

module Advent.Y19.Day22 where

import Data.List
import Data.Ratio
import Debug.Trace

data Instruction = Cut Integer | Incr Integer | Reverse

shuffle :: Integer -> Instruction -> Integer -> Integer
shuffle l (Cut c) pos = (pos - c) `mod` l
shuffle l (Incr i) pos = (i * pos) `mod` l
shuffle l Reverse pos = l - pos - 1

manyShuffles :: Integer -> [Instruction] -> Integer -> Integer
manyShuffles l instcns p = foldl (flip $ shuffle l) p instcns

parse :: String -> Instruction
parse s = case stripPrefix "deal with increment " s of
  Just ns -> Incr (read ns)
  Nothing -> case stripPrefix "cut " s of
    Just ns -> Cut (read ns)
    Nothing -> Reverse

part01 :: String -> IO ()
part01 fname = do
  input <- readFile fname
  let instrns = map parse $ lines input
  let rs = manyShuffles 10007 instrns 2019
  print $ show rs

modInv !a !c = if gcd == 1 then r else error "Error!" where (gcd,r,_) = extGCD a c

modPow :: Integer -> Integer -> Integer -> Integer
modPow a 0 m = 1
modPow !a !e !m = (a2 * r) `mod` m where
                 a2 = modPow a' e' m
                 a' = (a * a) `mod` m
                 e' = e `div` 2
                 r = if e `mod` 2 == 0 then 1 else a `mod` m

extGCD :: Integer -> Integer -> (Integer, Integer, Integer)
extGCD a b = go b a 0 1 1 0 where
  go 0 oldr news olds newt oldt =  (oldr, olds, oldt)
  go newr oldr news olds newt oldt = go newr' oldr' news' olds' newt' oldt' where
    (oldr',newr') = (newr, oldr - q * newr)
    (olds',news') = (news, olds - q * news)
    (oldt',newt') = (newt, oldt - q * newt)
    q = oldr `div` newr
    
composeInstructions :: Integer -> [Instruction] -> (Integer, Integer)
composeInstructions l = foldr compos (1,0) where
  compos ins !oldab = composeIt oldab $ getab ins
  getab (Cut c) = (1,c)
  getab (Incr i) = (modInv i l, 0)
  getab Reverse = (-1,l-1)
  composeIt (!a1,!b1) (!a2,!b2) = ((a1 * a2) `mod` l, ((a2 * b1) + b2) `mod` l)

solve02 :: Integer -> Integer -> [Instruction] -> Integer -> Integer
solve02 l times instns = rs where
  (a,b) = composeInstructions l instns
  an = modPow a times l
  asum = (an - 1) * (modInv (a-1) l)
  rs = \pos -> (an * pos + asum * b) `mod` l

part02 :: String -> IO ()
part02 fname = do
  input <- readFile fname
  let instrns = map parse $ lines input
  let rs = solve02 119315717514047 101741582076661 instrns 2020
  print $ rs

fname :: String
fname = "/home/t0xy/haskell/advent-haskell/inputs/2019/input22.txt"

fname1 :: String
fname1 = "/home/t0xy/haskell/advent-haskell/inputs/2019/tara-input22.txt"

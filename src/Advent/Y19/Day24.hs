{-# LANGUAGE MultiWayIf #-}
module Advent.Y19.Day24 where

import qualified Data.Map as M
import qualified Data.Set as S
import Control.Monad(join)
import Debug.Trace

data Tile = Bug | Empty deriving (Eq,Show,Ord)

type Point = (Int,Int)
type Layout = M.Map Point Tile

transform :: Layout -> Layout
transform layout = M.fromList newPointTiles where
  neighbours (x,y) = [(x-1,y),(x+1,y),(x,y-1),(x,y+1)]
  nbrsBugCount :: Point -> Int
  nbrsBugCount p = foldr
                   (\nbr bugCount ->
                       case M.lookup nbr layout of
                         Just Bug -> bugCount+1
                         _ -> bugCount)
                   0
                   (neighbours p)
  newPointTiles = map
                  (\(pt,tile) -> case (tile, nbrsBugCount pt) of
                                 (Bug, 1) -> (pt, Bug)
                                 (Bug, _) -> (pt, Empty)
                                 (Empty, c) -> if (c == 1 || c == 2)
                                               then (pt, Bug)
                                               else (pt, Empty))
                  (M.toList layout)

findRepeating :: Layout -> Layout
findRepeating layout = search S.empty layout where
  search :: S.Set Layout -> Layout -> Layout
  search set layout = case S.member layout set of
                        False -> search (S.insert layout set) (transform layout)
                        True -> layout

bioRating :: Layout -> Int
bioRating layout = foldr
                   (\((cno,rno), tile) total ->
                      case tile of
                        Empty -> total
                        Bug -> total + 2^(cno + (rno * 5)))
                   0
                   (M.toList layout)

showLayout :: Layout -> String
showLayout layout = do
  rno <- [0..4]
  let rowStr =
        map (\cno -> case M.lookup (cno,rno) layout of
                       Nothing -> ' '
                       Just Empty -> '.'
                       Just Bug -> '#')
        [0..4]
  rowStr ++ "\n"

printLayout :: Layout -> IO()
printLayout layout = do
  _ <- traverse (putStrLn) (lines (showLayout layout))
  return ()

readLayout :: String -> Layout
readLayout inp = M.fromList $ join rs where
  zipIndex :: [a] -> [(Int,a)]
  zipIndex = zip [0..]
  rs = map (\(lno,l) -> map (\(cno,c) -> case c of
                                '.' -> ((cno,lno),Empty)
                                '#' -> ((cno,lno),Bug)
                                _ -> error $ "Found " ++ show c)
                        (zipIndex l))
       (zipIndex $ lines inp)

part01 :: String -> IO()
part01 fname = do
  inpt <- readFile fname
  let layout = readLayout inpt
  putStrLn "Read the layout.."
  printLayout layout
  putStrLn("")
  putStrLn("Repeating..")
  let repeating = findRepeating layout
  printLayout repeating
  putStrLn("BioRating...")
  putStrLn $ show $ bioRating repeating

test1 = "/home/t0xy/haskell/advent-haskell/inputs/2019/test24-p1.txt"
input = "/home/t0xy/haskell/advent-haskell/inputs/2019/input24.txt"

type Depth = Int
type Minutes = Int
type Table = M.Map Depth Layout 

emptyLayout :: Layout
emptyLayout = M.fromList $ do
  rno <- [0..4]
  cno <- [0..4]
  return ((cno,rno),Empty)

data Side = TOP | BOT | LEFT | RIGHT deriving (Eq,Show)

transformIt :: (Layout, Layout, Layout) -> Layout
transformIt (outer, layout, inner) = M.fromList newPointTiles where
  xTop = -1
  xBot = 5
  xLeft = -1
  xRight = 5
  bugAt :: Layout -> Point -> Int
  bugAt lyt pt = case M.lookup pt lyt of
                    Just Bug -> 1
                    _ -> 0
  neighbours (x,y) = [(LEFT, (x-1,y)), (RIGHT, (x+1,y)), (TOP, (x,y-1)), (BOT, (x,y+1))]
  --unlike above this take the 'nbr point' instead of the 'main point'
  --thus someone else should check neighbours
  nbrBugCount :: (Side,Point) -> Int
  --the nbr is inner
  nbrBugCount (side, (2,2)) = case side of
                                 LEFT -> foldr (\rno tot -> tot + bugAt inner (4,rno)) 0 [0..4]
                                 RIGHT -> foldr (\rno tot -> tot + bugAt inner (0,rno)) 0 [0..4]
                                 TOP -> foldr (\cno tot -> tot + bugAt inner (cno,4)) 0 [0..4]
                                 BOT -> foldr (\cno tot -> tot + bugAt inner (cno,0)) 0 [0..4]
  --nbr is outer
  nbrBugCount (_, pt@(cno,rno)) = if | cno == xLeft -> bugAt outer (1,2)
                                     | cno == xRight -> bugAt outer (3,2)
                                     | rno == xTop -> bugAt outer (2,1)
                                     | rno == xBot -> bugAt outer (2,3)
                                     --nbr is not in inner or outer but within the curr layout
                                     | otherwise -> bugAt layout pt
  allNbrBugCount pt = foldr (\nbr tot -> tot + nbrBugCount nbr) 0 (neighbours pt)
  newPointTiles = map
                  (\(pt,tile) -> case (pt, tile, allNbrBugCount pt) of
                                   ((2,2),_,_) -> (pt, Empty)
                                   (_, Bug, 1) -> (pt, Bug)
                                   (_, Bug, _) -> (pt, Empty)
                                   (_, Empty, c) -> if (c == 1 || c == 2)
                                                    then (pt, Bug)
                                                    else (pt, Empty))
                  (M.toList layout)
                  
recursiveTransform :: Table -> Table
recursiveTransform prevMinTable = currMinTable where
  minDepth = (minimum $ M.keys prevMinTable) - 1
  maxDepth = (maximum $ M.keys prevMinTable) + 1
  triple :: Depth -> (Layout,Layout,Layout)
  triple depth = (at (depth-1), at depth, at (depth + 1)) where
    at d = case M.lookup d prevMinTable of
             Nothing -> emptyLayout
             Just layout -> layout
  go :: (Int -> Int) -> Int -> Int -> Table -> Table
  go next d to currTable = let layout = transformIt $ triple d
                               newTable = M.insert d layout currTable
                           in case d == to of
                                True -> newTable
                                False -> go next (next d) to newTable
  currMinOuterTable = go (\d -> d - 1) 0 minDepth M.empty
  currMinTable = go (\d -> d+1) 1 maxDepth currMinOuterTable
  

totalBugsCounter :: Table -> Int
totalBugsCounter table = foldr (\(_,layout) allTotal ->
                                  foldr (\(pt,tile) tot -> case (pt,tile) of
                                                             ((2,2),_) -> tot
                                                             (_,Bug) -> tot + 1
                                                             _ -> tot)
                                  allTotal
                                  (M.toList layout))
                         0
                         (M.toList table)
                         
part02 :: String -> IO()
part02 fname = do
  inpt <- readFile fname
  let layout = readLayout inpt
  putStrLn "Read the layout.."
  printLayout layout
  putStrLn("")
  let transformed = iterate (recursiveTransform) (M.singleton 0 layout)
  putStrLn "AT MINUTE 0........"
  printTable (transformed !! 0)
  putStrLn "................."
  putStrLn ""
  putStrLn "AT MINUTE 1........"
  printTable (transformed !! 1)
  putStrLn "................."
  putStrLn ""
  putStrLn "AT MINUTE 2........"
  printTable (transformed !! 2)
  putStrLn "................."
  putStrLn ""
  putStrLn "AT MINUTE 10........"
  printTable (transformed !! 10)
  putStrLn "................."
  putStrLn ""
  putStrLn "Sum after 10:"
  putStrLn $ show $ totalBugsCounter $ transformed !! 10
  putStrLn "Sum after 200:"
  putStrLn $ show $ totalBugsCounter $ transformed !! 200
  putStrLn $ show $ M.size $ transformed !! 200
  putStrLn "Sum after 201:"
  putStrLn $ show $ totalBugsCounter $ transformed !! 201
  putStrLn $ show $ M.size $ transformed !! 201

printTable :: Table -> IO ()
printTable table = mapM_ (\(d,layout) -> do
                             putStrLn $ "Depth " ++ show d
                             printLayout layout)
                   (M.toList table)

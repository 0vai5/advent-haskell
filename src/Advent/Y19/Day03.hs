module Advent.Y19.Day03 where

import Data.List.Split

type Point = (Int,Int)
data Axis = H | V
type Distance = Int
data Segment = S Axis Point Distance

segments :: String -> [Segment]
segments input = compute (0,0) $ splitOn "," input where
  compute :: Point -> [String] -> [Segment]
  compute _ [] = []
  compute prevPoint@(px,py) ((axis:dist):xs) = newSeg:compute newPoint xs where
    (newSeg, newPoint) = let d = read dist in case axis of
                                                'L' -> (S H prevPoint d, (px + d, py)) 
                                                'R' -> (S H prevPoint (-d), (px - d, py))
                                                'U' -> (S V prevPoint d, (px, py + d))
                                                'D' -> (S V prevPoint (-d), (px, py - d))

numInRange :: Int -> Int -> Int -> Bool
numInRange n s e = if s <= e
                   then s <= n && n <= e 
                   else numInRange n e s

orderInSameAxis :: (Int,Int) -> (Int,Int) -> ((Int,Int),(Int,Int))
orderInSameAxis (s1, e1) (s2, e2) | s1 > e1 = orderInSameAxis (e1,s1) (s2,e2)
                                  | s2 > e2 = orderInSameAxis (s1,e1) (e2,s2)
                                  | s1 < s2 = ((s1,e1),(s2,e2))
                                  | otherwise = ((s2,e2),(s1,e1))

intersects :: Segment -> Segment -> Maybe Point
intersects (S H (a,b) l) (S V (x,y) m) = if yes then Just (x,b) else Nothing
  where yes = numInRange x a (a+l) &&
              numInRange b y (y+m)
intersects sg1@(S V _ _) sg2@(S H _ _) = intersects sg2 sg1
intersects (S H (a,b) l) (S H (x,y) m) | b == y =
                                         let ((s1,e1),(s2,e2)) = orderInSameAxis (a,a+l) (x,x+m) in
                                           if numInRange e1 s2 e2 then Just (s2,b)
                                           else Nothing
                                       | otherwise = Nothing
intersects (S V (a,b) l) (S V (x,y) m) | a == x =
                                         let ((s1,e1),(s2,e2)) = orderInSameAxis (b,b+l) (y,y+m) in
                                           if numInRange e1 s2 e2 then Just (a,s2)
                                           else Nothing
                                       | otherwise = Nothing

allIntersections :: [Segment] -> [Segment] -> [Point]
allIntersections wire1 wire2 = do
  sg1 <- wire1
  sg2 <- wire2
  case intersects sg1 sg2 of
    Just p -> [p]
    Nothing -> []

shortest :: [Point] -> Maybe Point
shortest [] = Nothing
shortest (init:xs) = foldr (\c@(cx,cy) r@(Just (rx,ry)) ->
                                     let cd = abs cx + abs cy
                                         rd = abs rx + abs ry in
                                       if cd < rd then Just c else r)
                     (Just init)
                     xs

check01 s1 s2 = shortest $ filter (\(x,y) -> not $ x == 0 && y == 0) (allIntersections (segments s1) (segments s2))

solve01 :: String -> IO ()
solve01 fname = do
  input <- readFile fname
  let vals = lines input
  let rs = check01 (head vals) (head (tail vals)) in
    putStrLn $ show rs

distanceFromSegment :: Point -> Segment -> (Bool, Int)
distanceFromSegment (a,b) (S H (x,y) d) | b == y && numInRange a x (x + d) = (True, abs $ x - a)
                                        | otherwise = (False, abs d)
distanceFromSegment (a,b) (S V (x,y) d) | a == x && numInRange b y (y + d) = (True, abs $ y - b)
                                        | otherwise = (False, abs d)
                                        
stepsForWire :: Point -> [Segment] -> Int
stepsForWire p [] = error "Error!!"
stepsForWire p (x:xs) = if found
                        then steps
                        else steps + stepsForWire p xs where
  (found, steps) = distanceFromSegment p x
  

allSteps :: String -> String -> [Int]
allSteps s1 s2 = map (\p -> stepsForWire p w1 + stepsForWire p w2) intersections where
  w1 = segments s1
  w2 = segments s2
  intersections = filter (\(x,y) -> not $ x == 0 && y == 0) (allIntersections w1 w2)

check02 s1 s2 = minimum $ allSteps s1 s2

solve02 :: String -> IO ()
solve02 fname = do
  input <- readFile fname
  let vals = lines input
  let rs = check02 (head vals) (head (tail vals)) in
    putStrLn $ show rs

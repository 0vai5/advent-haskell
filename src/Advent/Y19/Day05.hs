module Advent.Y19.Day05 where

import Data.List.Split

identity :: a -> a
identity x = x

data OpCode = In | Out | Add | Mul | JiT | JiF | LessT | EQL | Halt deriving (Eq,Show)
data Mode = Position | Immediate deriving (Eq, Show)

type Code = (OpCode,Mode,Mode,Mode)

readCodeNModes :: Int -> Code
readCodeNModes code = (op, mode 0, mode 1, mode 2) where
  str = reverse (show code)
  op = case take 2 str of
         ('1':_) -> Add
         ('2':_) -> Mul
         ('3':_) -> In
         ('4':_) -> Out
         ('5':_) -> JiT
         ('6':_) -> JiF
         ('7':_) -> LessT
         ('8':_) -> EQL
         ('9':'9':_) -> Halt
         _ -> error str
  params = drop 2 str
  mode i = if i < length params
           then case params !! i of
                  '1' -> Immediate
                  _ -> Position
           else Position

ilength :: OpCode -> Int
ilength op | op `elem` [In,Out] = 2
           | op `elem` [JiT,JiF] = 3
           | op `elem` [Add,Mul,LessT,EQL] = 4
           | otherwise = 1

readByMode :: Mode -> Int -> [Int] -> Int
readByMode Immediate v _ = v
readByMode Position p instList = instList !! p

writeByMode :: Mode -> Int -> Int -> [Int] -> [Int]
writeByMode Immediate _ _ _ = error "Shouldn't happen as mentioned in instructions"
writeByMode Position pos val instList = replaceVal pos val instList 

liftVal xs = ((False,0),xs)

-- currInst does not contain code/operator as it is passed separately
-- so it only contains operands/parameters
opcodeExecutor :: Code -> [Int] -> [Int] -> IO ((Bool,Int),[Int])
opcodeExecutor (Halt, _, _, _) _ _ = return $ liftVal []
opcodeExecutor (In, m, _, _) currInst instList = do
  putStrLn "Enter input(1): "
  -- line <- getLine
  -- change input here for part1 and part2
  -- not reading it from console as my emacs get hang if reading from console
  let v = 5
  return $ liftVal $ writeByMode m (head currInst) v instList
opcodeExecutor (Out, m, _, _) currInst instList = do
  putStrLn $ show $ readByMode m (head currInst) instList
  return $ liftVal instList
opcodeExecutor (op, m1, m2, m3) currInst instList | op `elem` [Add,Mul,LessT,EQL] = return $ liftVal result where
                                                      func = case op of
                                                        Add -> (+)
                                                        Mul -> (*)
                                                        LessT -> (\x y -> if x < y then 1 else 0)
                                                        EQL -> (\x y -> if x == y then 1 else 0)
                                                      v1 = readByMode m1 (head currInst) instList
                                                      v2 = readByMode m2 (head $ tail currInst) instList
                                                      rs = v1 `func` v2
                                                      result = writeByMode m3 (head $ tail $ tail currInst) rs instList  
opcodeExecutor (op, m1, m2, _) currInst instList | op `elem` [JiT, JiF] = return (rs, instList) where
                                                      func = case op of
                                                        JiT -> (\v -> v /= 0)
                                                        JiF -> (\v -> v == 0)
                                                      v1 = readByMode m1 (head currInst) instList
                                                      rs = if func v1
                                                           then (True, readByMode m2 (head $ tail currInst) instList)
                                                           else (False, 0)
                                                      
                                                      
executor :: Int -> Int -> [Int] -> IO ()
executor pc currCode instList = do 
  let code@(op,_,_,_) = readCodeNModes currCode
  let l = ilength op
  let currInst = take l $ drop pc instList
  ((jumped, pcval),result) <- opcodeExecutor code (tail currInst) instList
  let newpc = if jumped then pcval else pc + l
  if result == []
    then return ()
    else executor newpc (result !! newpc) result

replaceVal :: Int -> a -> [a] -> [a]
replaceVal _ _ [] = []
replaceVal 0 val (x:xs) = val:xs
replaceVal pos val (x:xs) = x: replaceVal (pos-1) val xs

solve01 :: String -> IO ()
solve01 inputFile = do
  input <- readFile inputFile
  let vals = map read $ splitOn "," input
  executor 0 (head vals) vals

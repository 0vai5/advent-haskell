module Advent.Y19.Day04 where

check :: String -> Bool
check (x:xs) = go x xs 0 False where
  go prev (y:ys) c oneRep | prev == y = go y ys (c+1) oneRep 
                          | prev < y = go y ys 0 (oneRep || c == 1)
                          | otherwise = False
  go _ [] c oneRep = c == 1 || oneRep

generate :: Int -> Int -> [Int]
generate s e = [v | v <- [s .. e], check $ show v]

test = generate 356261 846303

module Advent.Y19.Day10 where

import Debug.Trace
import Data.Ratio

import Control.Monad(guard,join)
import qualified Data.Set as S

type Position = (Int,Int)

type Bounds = (Position,Position)

data Delta = D Int Int deriving (Show,Eq)

data Asteroids = Astr {
  asteroids :: S.Set Position,
  bounds :: Bounds}

getAllDeltas :: Int -> Int -> S.Set Delta
getAllDeltas totX totY = S.fromList allDeltas where
  fractions = S.fromList $ do
    dx <- [-totX .. totX]
    dy <- [-totY .. totY]
    guard( dx /= 0)
    return $ dy % dx
  deltas = do
    delta <- S.toList fractions
    let (x,y) = (denominator delta, numerator delta)
    [D x y, D (-x) (-y)]
  allDeltas = D 0 1:D 0 (-1):deltas

withinBounds :: Bounds -> Position -> Bool
withinBounds ((minX,minY),(maxX,maxY)) (x,y) = minX <= x && x <= maxX && minY <= y && y <= maxY

-- returns the first visible asteroid from the given position for the given delta
findVisible :: Asteroids -> Delta -> Position -> Maybe Position
findVisible astr (D dx dy) (x,y) = rs where
  rs = find (x+dx,y+dy)
  find pos@(x',y') | withinBounds (bounds astr) pos = if S.member pos (asteroids astr)
                                                      then Just pos
                                                      else find (x'+dx,y'+dy)
                   | otherwise = Nothing
  
-- returns all visible asteroids from the given position
allVisible :: Asteroids -> [Delta] -> Position -> [Position] 
allVisible astr deltas pos = rs where
  rs = do
    delta <- deltas
    case findVisible astr delta pos of
      Nothing -> []
      Just vsbl -> return vsbl

monitoringStation :: Asteroids -> [Delta] -> (Position,Int)
monitoringStation astr@(Astr asteroids _) alldeltas = station where
  visibles = allVisible astr alldeltas
  asteroidVisibleTuples = S.map (\pos -> (pos,(length . visibles) pos)) asteroids
  station = S.foldr (\el@(_,count) rs@(_,count') ->
                       if count > count'
                       then el
                       else rs)
                    ((0,0),-1)
                    asteroidVisibleTuples 

readData :: String -> Asteroids
readData input = Astr asteroids bounds where
  matrix :: [[(Int,Int,Char)]]
  matrix =  map (\(row,line) -> map (\(col,ch) -> (col,row,ch)) (zip [0..] line)) $ zip [0..] $ lines input
  asteroids = S.fromList $ map (\(r,c,_) -> (r,c)) $ filter (\(_,_,ch) -> ch == '#') $ join matrix
  bounds = ((0,0),(maxX - 1, maxY - 1))
  maxX = length $ head matrix
  maxY = length matrix

part01 :: String -> IO ()
part01 fname = do
  input <- readFile fname
  let theAsteroids@(Astr asteroids ((minX,minY),(maxX,maxY))) = readData input
  let orderedDeltas = S.toAscList $ getAllDeltas (maxX-minX) (maxY-minY)
  let rs = monitoringStation theAsteroids orderedDeltas
  putStrLn $ show rs

instance Ord Delta where
  (D col row) `compare` (D col' row') = (quad  col row) `compare` (quad col' row') where
    quad :: Int -> Int -> (Int, Rational)
    quad x y | x >= 0 && y < 0 = (0, slope x (-y))
             | x > 0 && y >= 0 = (1, slope y x)
             | x <= 0 && y > 0 = (2, slope (-x) y)
             | x < 0 && y <= 0 = (3, slope (-y) (-x))
    slope :: Int -> Int -> Rational
    slope y x | y >= 0 && x >= 0 = if x == 0 then toRational (minBound+y) else (toInteger y) % (toInteger x)

fire :: Asteroids -> [Delta] -> Int -> [Position]
fire astr@(Astr asteroids bounds) deltas count = join $ loop asteroids count where
  (monStn,_) = monitoringStation astr deltas
  loop remaining count | count <= 0 || S.size remaining == 0 = []
                       | otherwise = fired:loop nowRemaining nowCount where
                           fired = allVisible (Astr remaining bounds) deltas monStn
                           firedSet = S.fromList fired
                           nowRemaining = S.difference remaining firedSet
                           nowCount = count - (length fired)
                                              
part02 :: String -> IO ()
part02 fname = do
  input <- readFile fname
  let astr@(Astr _ ((minX,minY),(maxX,maxY))) = readData input
  let orderedDeltas = S.toAscList $ getAllDeltas (maxX-minX) (maxY-minY)
  let fired = fire astr orderedDeltas 200
  putStrLn "Fired at 200th: "
  putStrLn $ show $ fired !! 199
